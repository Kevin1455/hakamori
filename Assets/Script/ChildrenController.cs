﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Mirror;
using UnityEngine.SceneManagement;

public class ChildrenController : NetworkBehaviour
{
    // ターゲットオブジェクトの Transformコンポーネントを格納する変数
    public Transform target;
    private bool Firstescape;
    private bool Arrivalpoint;
    private Animator animator;
    private bool Onlineflg;//対戦モードかどうか
    NavMeshAgent agent;
    //RangeObject Rangeobject;
    bool Escapeflg = false;
    //オブジェクトの移動速度を格納する変数
    public float moveSpeed;
    // オブジェクトが停止するターゲットオブジェクトとの距離を格納する変数
    public float stopDistance;
    // オブジェクトがターゲットに向かって移動を開始する距離を格納する変数
    public float moveDistance;
    public bool Attackflg; // 墓を攻撃しているかどうか
    public bool Walkflg;   // 子供が墓を狙っているときの状態
    public bool Runflg;    // 子供が墓守から逃げているときの状態
    //AudioSource[] audioSource;
    AudioSource audioSource;
    public AudioClip walking_child;
    public AudioClip running_child;
    public AudioClip Breakinggrave;
    public AudioClip himei;
    private bool Audioflg;

    private GameObject Objectspawn;
    private bool Targetgraveflg;

    SkinnedMeshRenderer smr;
    Material Pickaxematerial;
    Material Body;
    Material Head;
    Color32 Substance;
    Color32 Translucent;
    private GameObject SkyChange;
    private bool Konoyo;
    private bool Frame;

    private GameObject Player;
    private GameObject Hand;

    private GameObject UICatch;
    // Start is called before the first frame update
    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        Firstescape = true;
        //audioSource = gameObject.GetComponents<AudioSource>();
        audioSource = gameObject.GetComponent<AudioSource>();
        Audioflg = true;
    }

    private void Start()
    {
        if (SceneManager.GetActiveScene().name == "OnlineScene")
        {
            Onlineflg = true;
        }
        else Onlineflg = false;

        Objectspawn = GameObject.Find("ObjectSpawn");
        Targetgraveflg = true;

        smr = GetComponentInChildren<SkinnedMeshRenderer>();
        //material = smr.materials;
        Body = smr.materials[0];
        Head = smr.materials[1];
        Substance = new Color32(255, 255, 255, 255);
        Translucent = new Color32(255, 255, 255, 50);
        SkyChange = GameObject.Find("SkyboxChange");
        Pickaxematerial = gameObject.GetComponentInChildren<MeshRenderer>().material;
        Frame = true;

        Player = GameObject.Find("Player");
        Hand = GameObject.Find("Hand");
        UICatch = GameObject.Find("UICamera/Canvas");

        Konoyo = SkyChange.GetComponent<SkyboxChange>().SkyChangeFlag();
        if (!Konoyo)
        {
            Body.SetOverrideTag("RenderType", "Transparent");
            Body.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            Body.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            Body.SetInt("_ZWrite", 0);
            Body.DisableKeyword("_ALPHATEST_ON");
            Body.EnableKeyword("_ALPHABLEND_ON");
            Body.DisableKeyword("_ALPHAPREMULTIPLY_ON");
            Body.renderQueue = 3000;

            Head.SetOverrideTag("RenderType", "Transparent");
            Head.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            Head.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            Head.SetInt("_ZWrite", 0);
            Head.DisableKeyword("_ALPHATEST_ON");
            Head.EnableKeyword("_ALPHABLEND_ON");
            Head.DisableKeyword("_ALPHAPREMULTIPLY_ON");
            Head.renderQueue = 3000;

            Body.color = Translucent;
            Head.color = Translucent;
            Pickaxematerial.color = Translucent;
        }
    }

    private void Update()
    {
        Konoyo = SkyChange.GetComponent<SkyboxChange>().SkyChangeFlag();
        
        if (Konoyo != Frame)
        {
            if(Konoyo)
            {
                Body.SetOverrideTag("RenderType", "");
                Body.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                Body.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                Body.SetInt("_ZWrite", 1);
                Body.DisableKeyword("_ALPHATEST_ON");
                Body.DisableKeyword("_ALPHABLEND_ON");
                Body.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                Body.renderQueue = -1;
                
                Head.SetOverrideTag("RenderType", "");
                Head.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                Head.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                Head.SetInt("_ZWrite", 1);
                Head.DisableKeyword("_ALPHATEST_ON");
                Head.DisableKeyword("_ALPHABLEND_ON");
                Head.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                Head.renderQueue = -1;

                Body.color = Substance;
                Head.color = Substance;
                Pickaxematerial.color = Substance;
            }
            else if (!Konoyo)
            {
                Body.SetOverrideTag("RenderType", "Transparent");
                Body.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                Body.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                Body.SetInt("_ZWrite", 0);
                Body.DisableKeyword("_ALPHATEST_ON");
                Body.EnableKeyword("_ALPHABLEND_ON");
                Body.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                Body.renderQueue = 3000;
                
                Head.SetOverrideTag("RenderType", "Transparent");
                Head.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                Head.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                Head.SetInt("_ZWrite", 0);
                Head.DisableKeyword("_ALPHATEST_ON");
                Head.EnableKeyword("_ALPHABLEND_ON");
                Head.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                Head.renderQueue = 3000;

                Body.color = Translucent;
                Head.color = Translucent;
                Pickaxematerial.color = Translucent;
            }
        }

        Frame = Konoyo;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Escapeflg)
        {
            // 墓守の視線が子供に向けられたとき
            Runflg = true;
            Walkflg = false;
            Targetgraveflg = true;
            // プレイヤーから逃げるオブジェクトを見つける処理
            if (Firstescape || Arrivalpoint)
            {
                ChangeTarget();
                Firstescape = false;
                Arrivalpoint = false;
            }
        }
        else
        {
            // 子供が墓を狙っているとき
            Walkflg = true;
            Runflg = false;
            //audioSource[1].Pause();
            Firstescape = true;

            if (!Onlineflg && Targetgraveflg) 
            {
                target = Objectspawn.GetComponent<ObjectSpawn>().GetTargetGrave();
                Targetgraveflg = false;
            }
            else if(Onlineflg && Targetgraveflg)
            {
                target = Objectspawn.GetComponent<ObjectSpawn>().GetOnlineTargetGrave();
                Targetgraveflg = false;
            }

            if(target == null) Targetgraveflg = true;

            if (!Onlineflg && !Targetgraveflg) agent.destination = target.transform.position;
            else if (Onlineflg && !Targetgraveflg) Move();
        }

        if (Attackflg)
        {
            Walkflg = false;
            //audioSource[0].Pause();
            //audioSource[1].Pause();

            // SEが重ならないように鳴らす
            if (!audioSource.isPlaying) audioSource.PlayOneShot(Breakinggrave, 1.0f);

            animator.SetBool("Hammer", true);
            animator.SetBool("Walk", false);
        }

        if (Walkflg)
        {
            animator.SetBool("Walk", true);
            animator.SetBool("Run", false);
            animator.SetBool("Hammer", false);
            //audioSource[1].Play();

            if (!audioSource.isPlaying) audioSource.PlayOneShot(walking_child, 1.0f);
        }

        if (Runflg)
        {
            animator.SetBool("Run", true);
            animator.SetBool("Walk", false);
            animator.SetBool("Hammer", false);
            //audioSource[0].Play();
            if (!audioSource.isPlaying) audioSource.PlayOneShot(running_child, 1.0f);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        //接触したオブジェクトのタグが"Player"のとき
        if (other.gameObject.tag == "Player")
        {
            Hand.GetComponent<HandController>().HandAnimation(false);

            Player.GetComponent<PlayerController>().SetCatchFlg(true);

            AudioSource.PlayClipAtPoint(himei, transform.position);

            Destroy(gameObject);
        }

        //接触したオブジェクトのタグが"gravestone"のとき
        if (other.gameObject.tag == "gravestone")
        {
            if (other.gameObject.transform.position == target.position)
            {
                Attackflg = true;
                if (Audioflg)
                {
                    audioSource.Stop();
                    Audioflg = false;
                }
            }
        }
        // 子供が墓を壊したら
        else if (other.gameObject.tag == "gravestone_02")
        {
            Attackflg = false;
            Targetgraveflg = true;
            //audioSource[1].UnPause();
            //audioSource[0].UnPause();
            if (!Audioflg)
            {
                audioSource.Stop();
                Audioflg = true;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Escape" && Escapeflg)
        {
            Arrivalpoint = true;
        }
    }

    public void ChangeTarget()
    {
        //Rangeobject = GetComponentInChildren<RangeObject>();
        //Targetescape = Rangeobject.GetEscapeTarget();
        //agent.destination = Targetescape;

        Attackflg = false;
        //audioSource[0].UnPause();
        //audioSource[1].UnPause();
        //Rangeobject = GetComponentInChildren<RangeObject>();
        //target = Rangeobject.GetEscapeTarget();
        if (!Onlineflg) agent.destination = target.transform.position;
        else Move();
    }

    public void SetEscapeFlg(bool Flg)
    {
        Escapeflg = Flg;
    }

    public void SetArrivalFlg(bool Flg)
    {
        Arrivalpoint = Flg;
    }

    public Vector3 GetTarget()
    {
        return target.transform.position;
    }

    public void SetOnlineFlg(bool Flg)
    {
        Onlineflg = Flg;
    }

    //[Command]
    [ServerCallback]
    private void Move()
    {
        agent.destination = target.transform.position;
    }
}
