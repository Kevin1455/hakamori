﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using Mirror;

public class GhostsController : NetworkBehaviour
{
    // ターゲットオブジェクトの Transformコンポーネントを格納する変数
    public Transform target;
    public Transform targetPly;
    public Transform targetEne;
    public GameObject GhostRag; //ラグドール
    private GameObject Enemyspawn;
    GameObject Tmp;     //死体生成
    NavMeshAgent agent;
    UIController UIcon;
    private bool Playerisdead;

    MeshRenderer mr;
    Material[] material;
    private GameObject SkyChange;
    private bool Konoyo;
    private bool Frame;
    private bool Cleanse;   //お化けの撃退アニメーション
    
    private bool Gameoverflg;
    private GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        UIcon = GameObject.Find("UICamera/Canvas").GetComponent<UIController>();
        Playerisdead = false;

        if (SceneManager.GetActiveScene().name != "OnlineScene")
        {
            player = GameObject.Find("Player");
            targetPly = GameObject.Find("Player").transform;
            target = GameObject.Find("Player").transform;
        }
        else
        {
            targetPly = GameObject.Find("Player(Network)(Clone)").transform;
            target = GameObject.Find("Player(Network)(Clone)").transform;
        }

        mr = GetComponentInChildren<MeshRenderer>();
        material = mr.materials;
        SkyChange = GameObject.Find("SkyboxChange");
        Enemyspawn = GameObject.Find("EnemySpawn");

        Konoyo = SkyChange.GetComponent<SkyboxChange>().SkyChangeFlag();

        if (Konoyo) material[0].color = new Color32(255, 255, 255, 0);

        Gameoverflg = false;
    }

    // Update is called once per frame
    void Update()
    {
        //オンラインシーン用ターゲット取得
        if(targetPly == null && SceneManager.GetActiveScene().name == "OnlineScene") targetPly = GameObject.Find("Player(Network)(Clone)").transform;
        if(target == null && SceneManager.GetActiveScene().name == "OnlineScene") target = GameObject.Find("Player(Network)(Clone)").transform;
        if(UIcon == null && SceneManager.GetActiveScene().name == "OnlineScene") UIcon = GameObject.Find("UICamera/Canvas").GetComponent<UIController>();

        Konoyo = SkyChange.GetComponent<SkyboxChange>().SkyChangeFlag();
        
        if(Konoyo != Frame)
        {
            if (Konoyo)
            {
                material[0].color = new Color32(255, 255, 255, 0);
            }
            else if (!Konoyo)
            {
                material[0].color = new Color32(255, 255, 255, 203);
            }
        }
        
        Frame = Konoyo;

        if (SceneManager.GetActiveScene().name != "OnlineScene" && !Gameoverflg) agent.destination = gameObject.transform.position;//target.transform.position;
        else if (Gameoverflg && player.GetComponent<PlayerController>().GetRotation())
        {
            SceneManager.sceneLoaded += GhostEndLoaded;
            SceneManager.LoadScene("GameOverScene");
        }
        //else CmdMove();
        target = targetPly;

        player.GetComponent<PlayerController>().SetGhostPosition(gameObject.transform);
    }
    void OnTriggerEnter(Collider other)
    {
        //接触したオブジェクトのタグが"Player"のとき
        if (other.gameObject.tag == "Player")
        {
            if (SceneManager.GetActiveScene().name == "OnlineScene")
            {
                Playerisdead = true;
            }
            else if(SceneManager.GetActiveScene().name == "TutorialScene")
            {
                SceneManager.LoadScene("TitleScene");
            }
            else
            {
                Enemyspawn.GetComponent<EnemySpawn>().enabled = false;
                agent.destination = gameObject.transform.position;
                player.GetComponent<PlayerController>().SetGhostPosition(gameObject.transform);
                Gameoverflg = true;
                //SceneManager.sceneLoaded += GhostEndLoaded;
                //SceneManager.LoadScene("GameOverScene");
            }
        }
        /*
        if(other.tag == "Ghost")
        {
            this.gameObject.transform.position = new Vector3(Random.Range(-65, 65), 0, Random.Range(-65, 65));
        }
        */
        /*
        if(other.tag == "Killer")
        {
            if(SceneManager.GetActiveScene().name == "OnlineScene") targetPly.GetComponent<NetworkPlayerMove>().SetUsedKiller();
            else targetPly.GetComponent<PlayerController>().SetUsedKiller();
            Destroy(this.gameObject);
        }*/
    }
    
    //ライトとの接触
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "MainCamera")
        {
            Tmp = Instantiate(GhostRag,Enemyspawn.transform);  //お化けの死体を生成
            //お化けが死んだ瞬間の座標・回転を保存
            Tmp.transform.position = gameObject.transform.position;
            Tmp.transform.rotation = gameObject.transform.rotation;
            Destroy(this.gameObject, 0);  //お化け本体を破壊
            Destroy(Tmp.gameObject, 1.9f);   //二秒後に破棄
        }
    }
        /*
        //接触したオブジェクトのタグが"MainCamera"のとき
        if (other.gameObject.tag == "MainCamera")
        {
            target = targetEne;
        }
        //接触したオブジェクトのタグが"Candle"のとき
        if (other.gameObject.tag == "Candle")
        {
            target = targetEne;
        }
     }
    */

    //[Command]
    [ServerCallback]
    private void CmdMove()
    {
        agent.destination = target.transform.position;
        if(Playerisdead)
        {
            UIcon.SetGameOvar(Playerisdead);
            Playerisdead = false;
        }
        //Debug.Log("Dead = " + Playerisdead);
    }
    
    private void GhostEndLoaded(Scene next, LoadSceneMode mode)
    {
        var GameOverManager
            = GameObject.FindGameObjectWithTag("GOManager").GetComponent<GameOverManager>();

        GameOverManager.Diverge = 1; 
        SceneManager.sceneLoaded -= GhostEndLoaded;
    }
}
