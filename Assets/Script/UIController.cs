﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Mirror;


public class UIController : MonoBehaviour
{
    AudioSource audiosource;//オーディオソース
    [SerializeField]
    private AudioClip WatchSE;//時計の針の音
    [SerializeField]
    private AudioClip ClearbookSE;//本の音
    [SerializeField]
    private AudioClip BellSE;
    [SerializeField]
    private AudioClip CrowSE;     //カラスの声

    private GameObject Timer;//時計のプレハブ
    private GameObject timerui;//時計のクローン
    private GameObject Longneedle;//長針のオブジェクト
    private GameObject Pausemenu;//メニュー画面UI
    private GameObject[] Graveuicln = new GameObject[5];//墓のUIのクローン配列5個
    private GameObject NearEffect;//墓守との距離のUI
    private GameObject Canvas;//Canvasオブジェクト//親オブジェクトに指定するため
    private GameObject mynetworkroommanager;//ネットワークルームマネージャー
    private GameObject gnrgrv;//ObjectSpawnerオブジェクト
    private GameObject EneSpawn;//敵の親オブジェクト

    private GameObject NetPly;//プレイヤーオブジェクト(対戦モード)
    private GameObject Chil;//子供(対戦モード)
    ObjectSpawn objectspawn;//ObjectSpawnスクリプト
    private NetworkCameraEffect Camef;//CameraEffectスクリプト

    private int Grvnum;//墓の残数
    private int Nowgrvnum;//墓の残数UI
    private int BellCount;//鐘が鳴った回数
    private bool Bellflg;//鐘を鳴らすフラグ
    private bool Clearflg;//クリアしたか
    private bool SpawnUIflg;//UIをすべて表示したか
    private bool Pauseflg;//メニュー画面を開いているか
    private bool Pushflg;//ボタンを押した
    private bool Onlinechildflg;//対戦モード クライアントフラグ
    private bool Onlinegameovarflg;//対戦モードのゲームオーバー演出フラグ
    private bool Scenechange;//対戦モード終了後タイトル画面へ戻る際に使うフラグ
    private bool Crowflg;//カラスが鳴くフラグ
    private float UIspawntime;//UIを表示する間隔
    private float Intervaltime;//時計の針が動く間隔
    private float CrowTime;//制限時間が20s前になったら
    private float CrowInterval;//カラスのインターバル
    private string[] UIname;//クリア時、勝敗決定時に表示されるUIの名前を格納

    public bool rot;//minuteが任意の値以上の値かどうか
    public float angle;//時計の針の角度
    public float minute;//時計の針が傾く間隔

    private bool Debugmode;//デバッグ用

    //ライトの電池残量の表示
    private GameObject batteryMAX;
    private GameObject battery3;
    private GameObject battery2;
    private GameObject battery1;
    private GameObject Nobattery;
    private float Lighttime = 0;
    private bool LightSwitch = false;
    private bool LightButtery = true;
    private GameObject Player;//世界の切り替え
    public GameObject Child;//世界の切り替え
    private bool LightSwitch2;
    //制限時間
    private float ClearTime = 0;

    private bool ChildCatch;

    void Awake()
    {
        Player = GameObject.Find("Player");
    }

    // Start is called before the first frame update
    void Start()
    {

        Debug.Log("Press G Key");

        //初期値のセット
        Grvnum = 0;
        Nowgrvnum = 0;
        BellCount = 0;
        UIname = new string[]{"UICamera/Canvas/Clearbook","Prefab/Winbook","Prefab/Losebook"};
        Bellflg = true;
        Clearflg = false;
        angle = 1.5f;
        minute = 0;
        Intervaltime = 0.5f;
        CrowTime = 20.0f;
        rot = false;
        SpawnUIflg = false;
        Pauseflg = false;
        Onlinechildflg = false;
        Onlinegameovarflg = false;
        Crowflg = false;

        batteryMAX = GameObject.Find("UICamera/Buttery/denti1");
        battery3   = GameObject.Find("UICamera/Buttery/denti2");
        battery2 = GameObject.Find("UICamera/Buttery/denti3");
        battery1 = GameObject.Find("UICamera/Buttery/denti4");
        Nobattery = GameObject.Find("UICamera/Buttery/denti5");
        battery3.SetActive(false);
        battery2.SetActive(false);
        battery1.SetActive(false);
        Nobattery.SetActive(false);
        LightSwitch2 = false;
        ChildCatch = false;

        //CanvasオブジェクトとObjectSpawnオブジェクトを格納
        Canvas = GameObject.Find("Canvas");
        gnrgrv = GameObject.Find("ObjectSpawn");
        Pausemenu = GameObject.Find("PauseMenu");
        if (SceneManager.GetActiveScene().name == "OnlineScene") NearEffect = GameObject.Find("UICamera/Canvas/NearEffect");
        else if(SceneManager.GetActiveScene().name != "TutorialScene") GameObject.Find(UIname[0]).SetActive(false);

        if (SceneManager.GetActiveScene().name == "PlayScene")
        {
            //Ghosts = GameObject.Find("Ghosts");
            //ChilSpawn = GameObject.Find("ChildrenSpawn");
            EneSpawn = GameObject.Find("EnemySpawn");
        }

        //時計のUIに関するオブジェクトの格納と生成
        Timer = (GameObject)Resources.Load("Prefab/Timer");
        timerui = Instantiate(Timer, new Vector3(0, 37.625f, 0), Quaternion.identity);       
        timerui.transform.SetParent(this.gameObject.transform, false);
        Longneedle = GameObject.Find("Longneedle");
        mynetworkroommanager = GameObject.Find("NetworkRoomManager");

        //メニューUIを非表示
        if(SceneManager.GetActiveScene().name != "TutorialScene")Pausemenu.SetActive(false);

        //ObjectSpawnスクリプトと墓の残数UIプレハブの格納
        objectspawn = gnrgrv.GetComponent<ObjectSpawn>();
        Camef = Camera.main.GetComponent<NetworkCameraEffect>();

        //SE関連の変数の初期化
        audiosource = GetComponent<AudioSource>();

        //時計の針のSEを格納
        WatchSE = (AudioClip)Resources.Load("Sound/watch_se");
        //クリア時のSEを格納
        ClearbookSE = (AudioClip)Resources.Load("Sound/gameclear_page");
        //鐘の音のSEを格納
        BellSE = (AudioClip)Resources.Load("Sound/BellSE");
        if (SceneManager.GetActiveScene().name == "OnlineScene")NearEffect.SetActive(false);
        //カラスのSEを格納
        CrowSE = (AudioClip)Resources.Load("Sound/CrowSE");

        Debugmode = false;
    }


    // Update is called once per frame
    void Update()
    {
        
        if(!Pauseflg) ClearTime += Time.deltaTime;
        //オンラインシーンの場合のオブジェクト取得
        if (SceneManager.GetActiveScene().name == "OnlineScene" && !Onlinechildflg)
        {
            Chil = GameObject.Find("Children(Network)(Clone)");
            Onlinechildflg = Chil.GetComponent<NetworkChildMove>().IsLocalPlayer();
            NetPly = GameObject.Find("Player(Network)(Clone)");
        }

        //UIを表示
        if (!SpawnUIflg && !Clearflg)SpawnGraveUI();
                
        if(!Clearflg)
        {
            //時計の針の動きの処理
            minute += Time.deltaTime;
            if (minute > Intervaltime)
            {
                rot = true;
                minute = 0;
            }
            else 
            {
                rot = false;
            }

            //カラスがゲームクリアを予見する処理
            if (minute > CrowTime)
            {
                Crowflg = true;
            }
            else

            //墓の残数UIの表示数を管理
            if (objectspawn.GetBreakGraveNum() <= Grvnum && SpawnUIflg)
            {
                Graveuicln[Grvnum].gameObject.SetActive(false);
                Grvnum--;
            }

            //Escまたはゲームパッド11ボタンを押したらメニュー画面
            MenuUI();

            //対戦モード子供用
            if(SceneManager.GetActiveScene().name == "OnlineScene") SearchEnemyAura();
        }
        //対戦モード クリア処理
        else if(SceneManager.GetActiveScene().name == "OnlineScene" && Clearflg) OnlineClear();
        //クリア処理
        else if (Clearflg) 
            { 
            Clear();
        }

        //3秒間隔に3回鐘のSEを鳴らす
        if(ClearTime >= 90 && Bellflg)
        {
            UIspawntime += Time.deltaTime;
            if(UIspawntime >= 4.0f) 
            { 
                audiosource.PlayOneShot(BellSE, 0.1f);
                UIspawntime = 0;
                BellCount++;
            }
            if(BellCount >= 6) Bellflg = false;
        }

        //制限時間経ったらクリア
        if(ClearTime >= 120 && !Clearflg && SceneManager.GetActiveScene().name != "TutorialScene")
        {
            ClearBook();
        }

        //カラスが鳴き始める
        if(ClearTime >= 90 && !Clearflg)
        {
            CrowInterval += Time.deltaTime;
            if(CrowInterval > 5.0f)
            {
                audiosource.PlayOneShot(CrowSE, 1.0f);
                CrowInterval = 0;
            }
        }

        //デバッグ用
        if(Input.GetKey(KeyCode.G) && !Debugmode)
        {
            Debugmode = true;
            Debug.Log("DebugMode = true"+" T+F = Fast timer"+" T+I+C = Srowtimer"+" P+K = Playertagchange"
                +"C+K = Children activefalse"+"G+K = Ghost activefalse"+"H = Debugmode false");
        }
        if(Debugmode)DebugMode();

        LightSwitch = Player.GetComponent<PlayerController>().LightSwitchflg();
        //ライトの時間
        if (LightSwitch == true && LightSwitch2 == false)
        {
            //電池の残量表示
            Lighttime += Time.deltaTime;
            if (Lighttime < 4)
            {
                batteryMAX.SetActive(true);
            }
            else if (4 <= Lighttime && Lighttime < 8)
            {
                batteryMAX.SetActive(false);
                battery3.SetActive(true);
            }
            else if (8 <= Lighttime && Lighttime < 12)
            {
                battery3.SetActive(false);
                battery2.SetActive(true);
            }
            else if (12 <= Lighttime && Lighttime < 16)
            {
                battery2.SetActive(false);
                battery1.SetActive(true);
            }
            else if ( Lighttime > 16)
            {
                battery1.SetActive(false);
                Nobattery.SetActive(true);
                LightButtery  = false;
            }
        }
        
        if(Pauseflg == true && SceneManager.GetActiveScene().name == "PlayScene")
        {
            if (Input.GetButton("Click"))
            {
              SceneManager.LoadScene(SceneManager.GetActiveScene().name); //同じステージの最初から
            }
            else if (Input.GetButton("Cancel"))
            {
                SceneManager.LoadScene("TitleScene");//タイトル画面遷移
            }
        }
        if(ChildCatch && SceneManager.GetActiveScene().name == "PlayScene")
        {
            Debug.Log("a");
        }
    }


    void LateUpdate()
    {
        //対戦モードなら値を共有する方を呼ぶ
        if(SceneManager.GetActiveScene().name == "OnlineScene") CmdMoveNeedle();
        else MoveNeedle();

        //UIの値を取得
        if(SceneManager.GetActiveScene().name == "OnlineScene" && SpawnUIflg &&!Clearflg)CmdGetSomething();

        //クリアフラグが立っていたら
        if (SceneManager.GetActiveScene().name == "OnlineScene" && Onlinegameovarflg)
        {
            ClearBook();
        }
    }


    private void MoveNeedle()
    {
        //時計の針を動かす処理
        if (rot && !Clearflg && !Pauseflg)
        {
            Longneedle.transform.rotation *= Quaternion.AngleAxis(angle, Vector3.back);
            //時計の針のSEを再生
            audiosource.PlayOneShot(WatchSE, 0.1f);
        }
    }
    
    
    //対戦モード 時計の針を動かし、UIの値をセット
    private void CmdMoveNeedle()
    {
        //時計の針を動かす処理
        if (rot && !Clearflg)
        {
            Longneedle.transform.rotation *= Quaternion.AngleAxis(angle, Vector3.back);
            //時計の針のSEを再生
            audiosource.PlayOneShot(WatchSE, 0.1f);     
        }
    }
    
    private void MenuUI()
    {
        //開く
        if (Input.GetButtonDown("Menu") && !Pauseflg && SceneManager.GetActiveScene().name == "PlayScene")
        {
            if(LightSwitch == true)
            {
                LightSwitch = false;
                LightSwitch2 = true;
            }
            GameObject.Find("Hand").GetComponent<HandController>().HandAnimation(false);
            GameObject.Find("Player").GetComponent<PlayerController>().SetHandFlg(true);
            GameObject.Find("Player").GetComponent<PlayerController>().enabled = false;
            GameObject.Find("Main Camera").GetComponent<CameraController>().SetMoveFlg(false);
            //GameObject.Find("Hand").GetComponent<HandController>().enabled = false;
            EneSpawn.SetActive(false);
            //フィールドに配置されていたオブジェクトを非表示
            audiosource.PlayOneShot(ClearbookSE, 0.8f);//SE再生
            Pausemenu.SetActive(true);
            Pauseflg = true;
        }
        //閉じる
        else if (Input.GetButtonDown("Menu") && Pauseflg && SceneManager.GetActiveScene().name == "PlayScene")
        {
            if (LightSwitch2 == true)
            {
                LightSwitch = true;
                LightSwitch2 = false;
            }
            audiosource.PlayOneShot(ClearbookSE, 0.8f);//SE再生
            //フィールドに配置されていたオブジェクトを再出現
            Pausemenu.SetActive(false);
            Pauseflg = false;
            GameObject.Find("Player").GetComponent<PlayerController>().SetHandFlg(false);
            GameObject.Find("Player").GetComponent<PlayerController>().enabled = true;
            GameObject.Find("Main Camera").GetComponent<CameraController>().SetMoveFlg(true);
            //GameObject.Find("Hand").GetComponent<HandController>().enabled = true;
            EneSpawn.SetActive(true);
        }
    }

    //対戦モード UI関連の値を取得
    private void CmdGetSomething()
    {
        int OldGrvnum = Grvnum;

        //クリア、ゲームオーバーは墓守視点
        Grvnum = gameObject.GetComponent<SelectPlayer>().GetGrvNum();//墓の残数
        Longneedle.transform.rotation = gameObject.GetComponent<SelectPlayer>().GetLRotate();//時計の長針
        Clearflg = gameObject.GetComponent<SelectPlayer>().GetClearFlg();//クリアフラグ
        Onlinegameovarflg = gameObject.GetComponent<SelectPlayer>().GetGameOvarFlg();//ゲームオーバーフラグ

        if (Grvnum < OldGrvnum) Graveuicln[OldGrvnum].gameObject.SetActive(false);//墓の残数UI
    }


    private void SearchEnemyAura()
    {
        if(Input.GetButton("Fire1") && Onlinechildflg)
        {
            //墓守が近くにいたらエフェクトを出す
            NearEffect.SetActive(true);

            float Range = Vector3.Distance(NetPly.transform.position, Chil.transform.position);

            Range = Range * 0.0003f;
            if(Range <= 0.01f)Range = 0.01f;
            if(Range >= 0.013f)Range = 0.013f;
            Range = 0.013f - Range;
                       
            NearEffect.GetComponent<Renderer>().material.color = new Color(212, 30, 56, Range);
        }
        else//目を閉じていない場合は消す
        {
            NearEffect.SetActive(false);
        }
    }


    //プレイ画面で1度だけ呼び出される
    void SpawnGraveUI()
    {
        GameObject Graveui = (GameObject)Resources.Load("Prefab/GraveUI");
        //墓の残数UIを表示する
        UIspawntime += Time.deltaTime;//表示間隔
       
        //表示間隔を満たしていて5個表示してないなら
        if (UIspawntime >= 0.1f && Grvnum < 5)
        {
            Graveuicln[Grvnum] = Instantiate(Graveui, new Vector3(90 * Grvnum, 0, 0), Quaternion.identity);
            Graveuicln[Grvnum].transform.SetParent(this.gameObject.transform, false);
            Grvnum++;
            UIspawntime = 0;
        }
        //フラグを落としGrvnumの値を-1する
        else if(Grvnum > 4)
        {
            Grvnum--;
            SpawnUIflg =true;
            UIspawntime = 0;
        }
    }
    //クリア処理を行う関数//UIの非表示と本の表示
    void ClearBook()
    {
        //オブジェクトの消去
        timerui.gameObject.SetActive(false);
        for (int grvnum = 0; grvnum < 5; grvnum++)
        {
            Destroy(Graveuicln[grvnum], 0.1f);
        }

        if (SceneManager.GetActiveScene().name == "OnlineScene")
        {
            DestroyObject();
        }
        else
        {
            GameObject.Find("EnemySpawn").GetComponent<EnemySpawn>().SetGhostFlg(false);
            //GameObject ghosts = GameObject.Find("Ghosts");
            GameObject childrenSpawn = GameObject.Find("EnemySpawn");
            //Destroy(ghosts, 0.1f);
            Destroy(childrenSpawn, 0.1f);
        }

        
        int UInum = 0;//表示UIの名前を選択
        
        if(SceneManager.GetActiveScene().name != "OnlineScene")
        {
            UInum = 0;//Clearbookを表示
            GameObject.Find(UIname[UInum]).SetActive(true);
        }
        else
        {
            if(gameObject.GetComponent<SelectPlayer>().GetGameOvarFlg()) UInum = 2;//Winbookを表示
            else UInum = 1;//Losebookを表示
            if(GameObject.Find("Children(Network)(Clone)").GetComponent<NetworkChildMove>().IsLocalPlayer())
            {
                UInum = gameObject.GetComponent<SelectPlayer>().VsResult(UInum);
            }
            //gameObject.GetComponent<SelectPlayer>().SetGameOvarFlg(false);///////
            Onlinegameovarflg = false;
            //クリアウィンドウ(本)を配置し表示
            GameObject clearbook = (GameObject)Resources.Load(UIname[UInum]);
            GameObject clrbookui =  Instantiate(clearbook, new Vector3(0, -1080.0f, 0), Quaternion.identity);
            clrbookui.transform.SetParent(Canvas.transform, false);
        }
        audiosource.PlayOneShot(ClearbookSE, 0.8f);//SE再生
        Clearflg = true;
        rot = true;
    }

    [ServerCallback]
    private void DestroyObject()
    {
        GameObject ghosts = GameObject.Find("Ghosts");
        GameObject childrenSpawn = GameObject.Find("ChildrenSpawn");
        Destroy(ghosts, 0.1f);
        Destroy(childrenSpawn, 0.1f);
    }

    //クリア処理を行う関数//墓の残数UI表示//1回//とカーソル移動の処理
    void Clear()
    {
        if (SpawnUIflg)
        {
            //墓の残数UIを表示する
            UIspawntime += Time.deltaTime;//表示間隔
            if (UIspawntime >= 1.0f && rot)
            {
                if (Grvnum >= Nowgrvnum)
                {
                    GameObject Graveui = (GameObject)Resources.Load("Prefab/GraveUI");
                    //墓の残数分UIを表示する
                    for (Nowgrvnum = 0; Grvnum >= Nowgrvnum; Nowgrvnum++)
                    {
                        if(Nowgrvnum < 3)Graveuicln[Nowgrvnum] = Instantiate(Graveui, new Vector3(90 * Nowgrvnum + 120, -450, 0), 
                            Quaternion.identity);
                        else if(Nowgrvnum >= 3)Graveuicln[Nowgrvnum] = Instantiate(Graveui, new Vector3(90 * Nowgrvnum - 100, -600, 0), 
                            Quaternion.identity);
                        Graveuicln[Nowgrvnum].transform.SetParent(Canvas.transform, false);
                    }
                }
                //墓の残数UIが初期値のままならPerfectの文字を表示
                else if(UIspawntime >= 1.5f && Nowgrvnum == 5)
                {
                    GameObject Perfecttext = (GameObject)Resources.Load("Prefab/ClearText");
                    GameObject Perfecttextcln = Instantiate(Perfecttext, new Vector3(0, 0, 0), Quaternion.identity);
                    Perfecttextcln.transform.SetParent(Canvas.transform, false);
                    Nowgrvnum++;
                }
                //写真表示
                else if(UIspawntime >= 1.8f)
                {
                    GameObject Picture;
                    GameObject Picturecln;
                    //ランダムに1枚
                    switch (Random.Range(0, 2)) 
                    {
                        case 0:
                            Picture = (GameObject)Resources.Load("Prefab/Clearpicture");
                            Picturecln = Instantiate(Picture, new Vector3(0, 0, 0), Quaternion.identity);
                            Picturecln.transform.SetParent(Canvas.transform, false);
                            break;
                        case 1:
                            Picture = (GameObject)Resources.Load("Prefab/Clearpicture2");
                            Picturecln = Instantiate(Picture, new Vector3(0, 0, 0), Quaternion.identity);
                            Picturecln.transform.SetParent(Canvas.transform, false);
                            break;
                    }
                    rot = false; 
                }

                if(StageChange.Stagenum == 1 && PlayerPrefs.GetInt("NResult") < Grvnum + 1)
                {
                    PlayerPrefs.SetInt("EResult", Grvnum + 1);
                    PlayerPrefs.Save();
                }
                else if (StageChange.Stagenum == 2 && PlayerPrefs.GetInt("NResult") < Grvnum + 1)
                {
                    PlayerPrefs.SetInt("NResult", Grvnum + 1);
                    PlayerPrefs.Save();
                }
                else if (StageChange.Stagenum == 3 && PlayerPrefs.GetInt("NResult") < Grvnum + 1)
                {
                    PlayerPrefs.SetInt("HResult", Grvnum + 1);
                    PlayerPrefs.Save();
                }
            }
        }

        //クリア後にプレイヤーとカメラ停止
        GameObject.Find("Player").GetComponent<PlayerController>().enabled = false;
        GameObject.Find("Hand").GetComponent<HandController>().enabled = false;
        GameObject.Find("Main Camera").GetComponent<CameraController>().SetMoveFlg(false);
        //選択
        if (Input.GetButton("Click") && UIspawntime >= 3)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name); //同じステージの最初から
        }
        else if (Input.GetButton("Cancel") && UIspawntime >= 3)
        {
            SceneManager.LoadScene("TitleScene");//タイトル画面遷移
        }
    }

    //対戦モードの場合、左クリックかRボタンを押されたらタイトル画面へ
    private void OnlineClear()
    {
        if(UIspawntime <= 20)UIspawntime += Time.deltaTime;
        if((UIspawntime >= 3 && Input.GetButton("Fire1") || UIspawntime >= 20) && !Onlinechildflg)
        {
            mynetworkroommanager.GetComponent<MyNetworkRoomManager>().FinishBattle();
        }
    }


    //SelectPlayerのゲームオーバーフラグをセット
    public void SetGameOvar(bool Flg)
    {
        gameObject.GetComponent<SelectPlayer>().SetGameOvarFlg(Flg);
    }

    private void DebugMode()
    {
        if(Input.GetKey(KeyCode.T) && Input.GetKey(KeyCode.F))
        {
            Intervaltime = 0.0f;
            Debug.Log("Timer interval = 0");
        }
        if(Input.GetKey(KeyCode.T) && Input.GetKey(KeyCode.I) && Input.GetKey(KeyCode.C))
        {
            Intervaltime = 60.0f;
            Debug.Log("Timer interval = 60");
        }
        if (Input.GetKey(KeyCode.P) && Input.GetKey(KeyCode.K))
        {
            GameObject Player = GameObject.Find("Player");
            Player.tag = "Untagged";
            Player.GetComponent<PlayerController>().Speed = 20;
            Debug.Log("Player is Untagged");
        }
        if(Input.GetKey(KeyCode.C) && Input.GetKey(KeyCode.K))
        {
            EneSpawn.SetActive(false);
            Debug.Log("Children had disappeared");
        }
        if(Input.GetKey(KeyCode.G) && Input.GetKey(KeyCode.K))
        {
            //Ghosts.SetActive(false);
            Debug.Log("Ghost had disappeared");
        }

        if(Input.GetKey(KeyCode.H) && Debugmode)
        {
            Intervaltime = 0.5f;
            GameObject Player = GameObject.Find("Player");
            Player.tag = "Player";
            EneSpawn.SetActive(true);
            Debugmode = false;
            Debug.Log("Debugmode = false");
        }
    }
    //ライトのバッテリーがあるかどうかの関数
    public bool LightButteryflg()
    {
        return LightButtery;
    }
    public bool ChildCatchflg()
    {
        return ChildCatch;
    }
}
