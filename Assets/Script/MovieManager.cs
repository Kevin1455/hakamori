﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

//欠落したコンポーネントを補填　セットアップエラーを回避
[RequireComponent(typeof(RawImage), typeof(VideoPlayer), typeof(AudioSource))]
//オープニングの動画を制御する
public class MovieManager : MonoBehaviour
{
    public VideoPlayer OP;    //オープニング再生用
    GameObject Fade;          //Fadeオブジェクト
    RawImage Screen;          //スクリーン
    AudioSource source;
    bool MovieStart;          //動画が開始されたか否か
    bool MovieCheck;          //能動的に動画を止めたら
    
    void Start()
    {
        Cursor.visible = false;
        Fade = GameObject.Find("Fade");    //Fadeを取得
        Screen = GetComponent<RawImage>(); //スクリーンにRawImageを割りあて
        OP = GetComponent<VideoPlayer>();  //VideoPlayerコンポーネントを取得
        source = GetComponent<AudioSource>();  //AudioSourceコンポーネントを取得

        MovieStart = false;                //動画が再生されたか否か
        OP.EnableAudioTrack(0, true);
        OP.SetTargetAudioSource(0, source);
        MovieCheck = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (OP.isPlaying) MovieStart = true; //動画が再生中

        //スクリーンにオープニングを描写
        if (OP.isPrepared) Screen.texture = OP.texture;
        //オープニングスキップ
        if ( Input.GetButtonDown("Fire1") || Input.GetButtonDown("Fire2") || Input.GetButtonDown("Fire3"))
        {
            MovieCheck = true;       //動画停止
        }
        //キーボード用スキップ
        if (Input.anyKeyDown)
        {
            MovieCheck = true;       //動画停止
        }

        //終了・停止検知
        if (MovieStart && (!OP.isPlaying || MovieCheck)) //動画が開始されてて動画が停止されたら
        {
            //フェードアウト タイトルコールにフェードアウト
            FadeManager.Instance.LoadScene("TitleCall", 1.0f);

        }
    }
}


