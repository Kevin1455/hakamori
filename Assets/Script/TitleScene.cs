﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleScene : MonoBehaviour
{
    public CameraController CameraCRE;//CameraEffectのコンポーネント
    public PlayerController PlayCRE;//CameraEffectのコンポーネント
    Animator animator;//タイトルロゴのアニメーター
    Animator Canimator;//操作説明のアニメーター
    GameObject Titlelogo;//タイトルロゴ
    GameObject TitleCnt;//操作説明
    GameObject Menu;//ゲーム終了確認ウィンドウ
    private bool Menuflg;//ゲーム終了確認ウィンドウの表示
    float Displaytime;//表示時間
    private GameObject CREDIT;
    private bool Tflg;
    private float MabatakiCnt;//瞬きの間は受け付けない
    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Menuflg = false;
        Titlelogo = GameObject.Find("Titlelogo3");//タイトルロゴオブジェクトを格納
        TitleCnt = GameObject.Find("Controller");//操作説明オブジェクトを格納
        Menu = GameObject.Find("WindowCamera/EndGame");//確認ウィンドウを格納
        Menu.SetActive(false);
        animator = Titlelogo.GetComponent<Animator>();//タイトルロゴのアニメーターを取得
        Canimator = TitleCnt.GetComponent<Animator>();//操作説明のアニメーターを取得
        Displaytime = 0;//初期値0
        CREDIT = GameObject.Find("CREDIT");
        MabatakiCnt = 1;
    }

    // Update is called once per frame
    void Update()
    {
        Displaytime += Time.deltaTime;

        //最初の5秒か25秒後経過後に操作説明を消してタイトルロゴを表示
        if(Displaytime >= 25)
        {
            animator.SetBool("TitleAnm", false);//タイトルロゴのアニメーション
            Canimator.SetBool("TitleCnt Bool", false);//操作説明のアニメーション
            Displaytime = 0;//時間をリセット
        }
        else if(25 > Displaytime && Displaytime >= 5)
        {
            TitleLogoAnim();
        }
        //まばたきボタンを押されたら
        if (Input.GetButtonDown("Fire1"))
        {
            TitleLogoAnim();
            MabatakiCnt = 0;
            Tflg = true;
        }
        if(Tflg)
        {
            MabatakiCnt += Time.deltaTime;
           if(MabatakiCnt >= 0.7f)
            {
                Tflg = false;
            }
        }
        //ゲーム終了
        if (Input.GetButtonDown("Menu") && !Menuflg && !CREDIT.GetComponent<CREDITcontroller>().CREDITflg() && MabatakiCnt >= 0.6f)
        {
            GameObject.Find("Hand").GetComponent<HandController>().HandAnimation(false);
            GameObject.Find("Player").GetComponent<PlayerController>().SetHandFlg(true);
            GameObject.Find("Player").GetComponent<PlayerController>().enabled = false;
            GameObject.Find("Main Camera").GetComponent<CameraController>().SetMoveFlg(false);
            //GameObject.Find("Hand").GetComponent<HandController>().enabled = false;
            //ウィンドウを開く
            Menu.SetActive(true);
            Menuflg = true;

        }
        else if (Input.GetButtonDown("Menu") && Menuflg)
        {
            GameObject.Find("Player").GetComponent<PlayerController>().SetHandFlg(false);
            GameObject.Find("Player").GetComponent<PlayerController>().enabled = true;
            GameObject.Find("Hand").GetComponent<HandController>().enabled = true;
            GameObject.Find("Main Camera").GetComponent<CameraController>().SetMoveFlg(true);
            //ウィンドウを閉じる
            Menu.SetActive(false);
            Menuflg = false;
        }
        if(Menuflg == true)
        {
            if (Input.GetButton("Click"))
            {
                Application.Quit();//ゲーム終了
                Debug.Log("Quit!!!");
            }
            else if (Input.GetButton("Cancel"))
            {
                GameObject.Find("Player").GetComponent<PlayerController>().SetHandFlg(false);
                GameObject.Find("Player").GetComponent<PlayerController>().enabled = true;
                GameObject.Find("Main Camera").GetComponent<CameraController>().SetMoveFlg(true);
                //ウィンドウを閉じる
                Menu.SetActive(false);
                Menuflg = false;
                Debug.Log("NoButton " + Menuflg);
            }
        }
    }

    private void TitleLogoAnim()
    {
            animator.SetBool("TitleAnm", true);//タイトルロゴのアニメーション
            Canimator.SetBool("TitleCnt Bool", true);//操作説明のアニメーション
    }
    public bool MenuFlgflg()
    {
        return Menuflg;
    }
}
