﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//タイトルコールでボタンを押したらタイトルに遷移
public class TitleCall : MonoBehaviour
{
    float BackTime;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        BackTime = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        BackTime += Time.deltaTime; //経過時間
        //何かボタンおしたらステージセレクト(旧TitleScene)
        if (Input.GetButtonDown("Fire1") || Input.GetButtonDown("Fire2") || Input.GetButtonDown("Fire3"))
        {
            FadeManager.Instance.LoadScene("TitleScene", 1.0f);
        }
        //キーボード用スキップ
        if (Input.anyKeyDown)
        {
            FadeManager.Instance.LoadScene("TitleScene", 1.0f);
        }

        //20s経過したら
        if (BackTime >= 20.0f)
        {
            FadeManager.Instance.LoadScene("Opening", 1.0f); // OP再生シーンに遷移
        }
    }
}
