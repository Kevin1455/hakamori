﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{
    //public Transform bookpen;
    //private int SceneManage = 0;
    //int Rencon = 0;
    public GameObject Children;   //子供のオブジェクト
    public GameObject Ghost;      //お化けのオブジェクト
    public GameObject Grave;      //墓のオブジェクト
    public GameObject Brokengrave;//壊れた墓のオブジェクト
    public GameObject Flash;      //ライトのオブジェクト

    public int Diverge;           //GameOverのパターン(受け取り)
    public int FlashCnt = 0;          //ライトの点滅
    // Start is called before the first frame update
    void Start()
    {
        //bookpen = GameObject.Find("bookpen").transform;
        if (Diverge == 1) GhostEnd();         //お化けエンド
        else if (Diverge == 2) ChildEnd();    //子供エンド
        else Debug.Log("エラー：Diverge =" + Diverge); //エラー
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Click"))
        {
            SceneManager.LoadScene("PlayScene"); //同じステージの最初から
        }
        else if (Input.GetButton("Cancel"))
        {
            SceneManager.LoadScene("TitleScene");//タイトル画面遷移
        }
        //if (Input.GetAxis("JoyUD") >= 0.5f)
        //{
        //    Rencon += 1;
        //    if (Rencon == 1 && SceneManage == 1)
        //    {
        //        SceneManage = 0;
        //        bookpen.transform.position += new Vector3(0, 0.55f, 0);
        //    }
        //}
        //else if (Input.GetAxis("JoyUD") <= -0.5f)
        //{
        //    Rencon += 1;
        //    if (Rencon == 1 && SceneManage == 0)
        //    {
        //        SceneManage = 1;
        //        bookpen.transform.position += new Vector3(0, -0.55f, 0);
        //    }
        //}
        //else
        //{
        //    Rencon = 0;
        //}
        //if (Input.GetButton("Fire1"))
        //{
        //    switch (SceneManage)
        //    {
        //        case 0:
        //            SceneManager.LoadScene("PlayScene");
        //            break;
        //        case 1:
        //            SceneManager.LoadScene("TitleScene");
        //            break;
        //    }
        //}

        if (Diverge == 1)  //お化けエンドのみ起動
        {
            FlashCnt++;
            if (FlashCnt % 18 > 0)
            {
                Flash.SetActive(false);
            }
            else
            {
                Flash.SetActive(true);
            }
            if (FlashCnt > 180) FlashCnt = 0;
        }
    }
    public void GhostEnd()    //お化けエンド用のオブジェクト群を真に
    {
        Ghost.SetActive(true);
        Grave.SetActive(true);
        Children.SetActive(false);
        Brokengrave.SetActive(false);
    }
    public void ChildEnd()    //子供エンドのオブジェクト群を真に
    {
        Ghost.SetActive(false);
        Grave.SetActive(false);
        Children.SetActive(true);
        Brokengrave.SetActive(true);
    }

}

