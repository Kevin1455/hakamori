﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyboxChange : MonoBehaviour
{
    public Material[] Sky;
    public bool Konoyo;
    private int num = 0;

    private void Start()
    {
        Konoyo = true;
    }

    private void FixedUpdate()
    {
        RenderSettings.skybox = Sky[num];
    }

    public void ChangeMaterial()
    {
        if(Konoyo)
        {
            Konoyo = false;
            num = 1;
        }
        else if(!Konoyo)
        {
            Konoyo = true;
            num = 0;
        }
    }
    public bool SkyChangeFlag()
    {
        return Konoyo;
    }


    //public float Transtime; // 切り替える時間の速度を調節する
    //
    //private Light Scenelight; // 変更前のライト
    //public Light Alternatelight; // 変更後のライト
    //public float Altfogdensity; // フォグの密度指数
    //public Color Altfogcolor; // フォグの色
    //
    //private Material Defaultsky; // デフォルトのスカイボックス
    //public Material Alternatesky; // 変更後のスカイボックス
    //private Material TempSky; // 生成するスカイボックスを入れる器
    //
    //// Start is called before the first frame update
    //void Start()
    //{
    //    // スカイボックスで使用されるライト
    //    Scenelight = RenderSettings.sun;
    //
    //    // 変更後のライトが設定されていたら
    //    if (Alternatelight != null)
    //    {
    //        // 非表示にする
    //        Alternatelight.gameObject.SetActive(false);
    //    }
    //
    //    // グローバルで使用されるスカイボックス
    //    Defaultsky = RenderSettings.skybox;
    //
    //    // マテリアルインスぺクターで設定されたプロパティから純粋にスカイボックスを生成する
    //    TempSky = new Material(Shader.Find("Skybox/Procedural"));
    //    RenderSettings.skybox = TempSky;
    //
    //    // 変更可能なスカイボックスにゲームスタート時に使用するスカイボックスと同じデータを入れる
    //    RenderSettings.skybox.Lerp(RenderSettings.skybox, Defaultsky, 1);
    //    
    //    // ライトの位置を変える
    //    StartCoroutine(AltLightTrans());
    //
    //    // ライトの色を変える
    //    //StartCoroutine(AltFogTrans());
    //
    //    // スカイボックスを変える
    //    StartCoroutine(AltSkyTrans());
    //}
    //
    //// Update is called once per frame
    //void Update()
    //{
    //
    //}
    //
    //// 再生モード停止時に呼び出される関数
    //void OnApplicationQuit()
    //{
    //    // 初期のスカイボックスに戻す
    //    RenderSettings.skybox = Defaultsky;
    //}
    //
    //IEnumerator AltLightTrans()
    //{
    //    float time = 0f;
    //
    //    // ライトの輝度、色、位置、影の暗さが変わりきるまで回す
    //    while (time < 1.0f)
    //    {
    //        // 照明レンダリング設定で指定されたライトをインスペクターで指定された代替ライトに変える
    //        Scenelight.intensity = Mathf.Lerp(Scenelight.intensity, Alternatelight.intensity, time);
    //        Scenelight.color = Color.Lerp(Scenelight.color, Alternatelight.color, time);
    //        Scenelight.transform.rotation = Quaternion.Lerp(Scenelight.transform.rotation, Alternatelight.transform.rotation, time);
    //        Scenelight.shadowStrength = Mathf.Lerp(Scenelight.shadowStrength, Alternatelight.shadowStrength, time);
    //
    //        // Transtimeによって時間をかける
    //        time += (Transtime * Time.deltaTime) / 1000;
    //
    //        yield return null;
    //    }
    //}
    //
    //IEnumerator AltFogTrans()
    //{
    //    float time = 0f;
    //
    //    // フォグの密度指数と色が変わりきるまで回す
    //    while (time < 1.0f)
    //    {
    //        RenderSettings.fogDensity = Mathf.Lerp(RenderSettings.fogDensity, Altfogdensity, time);
    //        RenderSettings.fogColor = Color.Lerp(RenderSettings.fogColor, Altfogcolor, time);
    //
    //        // Transtimeによって時間をかける
    //        time += (Transtime * Time.deltaTime) / 1000;
    //
    //        yield return null;
    //    }
    //}
    //
    //IEnumerator AltSkyTrans()
    //{
    //    float time = 0f;
    //
    //    // スカイボックスが変わりきるまで回す
    //    while (time < 1.0f)
    //    {
    //        RenderSettings.skybox.Lerp(RenderSettings.skybox, Alternatesky, time);
    //
    //        // Transtimeによって時間をかける
    //        time += (Transtime * Time.deltaTime) / 1000;
    //
    //        yield return null;
    //    }
    //}
}
