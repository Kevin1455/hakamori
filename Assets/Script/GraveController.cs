﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class GraveController : NetworkBehaviour
{
    float Breakgravetime; // 墓が壊れるまでの時間を計測
    [SerializeField]
    float Repairgravetime; // 墓を直すのにかかる時間を計測
    bool Onlineflg;//対戦モードフラグ
    [SerializeField]
    GameObject Brokengravestone01; // 壊れた墓のゲームオブジェクト
    [SerializeField]
    GameObject soul01;  // 墓が壊れた時の魂のゲームオブジェクト
    [SerializeField]
    GameObject Ghost; // お化けのゲームオブジェクト
    GameObject Objectspawn; // ObjectSpawnのスクリプトを呼び出す用
    GameObject Smoke;

    private new ParticleSystem[] particleSystem;

    AudioSource audioSource;
    //public AudioClip Breakinggrave;
    public AudioClip Underrepair;
    public AudioClip Repaircomplete;

    private GameObject SkyChange;
    private GameObject GraveHP;
    private float Interval;
    private bool Konoyo;
    private bool Frame;

    void Start()
    {
        // GameObjectのObjectSpawnを見つける
        Objectspawn = GameObject.Find("ObjectSpawn");

        // オンラインシーンかどうかをフラグで判断する
        if(Objectspawn.GetComponent<ObjectSpawn>().GetOnlineFlg()) Onlineflg = true;
        else Onlineflg = false;

        // 初期値を入れる
        Breakgravetime = 0;

        // Resourcesのプレハブにある壊れた墓のデータを読み込む
        if (!Onlineflg)Brokengravestone01 = (GameObject)Resources.Load("Prefab/gravestone_02");

        // Resourcesのプレハブにある魂のデータを読み込む
        if (!Onlineflg)soul01 = (GameObject)Resources.Load("Prefab/soul");

        // Resourcesのプレハブにあるお化けのデータを読み込む
        if (!Onlineflg)Ghost = (GameObject)Resources.Load("Prefab/Ghost");

        // audioSourceの取得
        audioSource = GetComponent<AudioSource>();

        Smoke = (GameObject)Resources.Load("Prefab/DustSmoke");

        particleSystem = GetComponentsInChildren<ParticleSystem>();

        SkyChange = GameObject.Find("SkyboxChange");
        GraveHP = transform.Find("gravestoneHP").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        Konoyo = SkyChange.GetComponent<SkyboxChange>().SkyChangeFlag();

        if(Konoyo != Frame)
        {
            Interval += Time.deltaTime;

            if (Konoyo)
            {
                GraveHP.SetActive(false);
                Interval = 0;
                Frame = Konoyo;
            }
            else if (!Konoyo && Interval >= 0.5f)
            {
                GraveHP.SetActive(true);
                Interval = 0;
                Frame = Konoyo;
            }
        }

        // もし、墓が壊れるまでの時間が20秒以上になったら
        if (Breakgravetime >= 20)
        {
            // ObjectSpawnの関数を呼ぶ
            Objectspawn.GetComponent<ObjectSpawn>().BreakGrave(gameObject);

            // 時間を0にリセットする
            Breakgravetime = 0;

            // 壊れた墓、魂を呼び出す
            if (gameObject.name == "gravestone_01(Clone)")
            {
                Instantiate(Brokengravestone01, gameObject.transform.position, gameObject.transform.rotation);
                Instantiate(soul01, gameObject.transform.position, gameObject.transform.rotation);
            }
            if (gameObject.name == "gravestone_01(Network)(Clone)")
            {
                CmdSpawner(Brokengravestone01, gameObject.transform.position, gameObject.transform.rotation);
                CmdSpawner(soul01, gameObject.transform.position, gameObject.transform.rotation);
            }

            Vector3 Smokepos = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 1, gameObject.transform.position.z);
            // 墓の周りに土煙を表示
            Instantiate(Smoke, Smokepos, Quaternion.identity);

            /*
            // お化けを呼び出す
            if(Onlineflg)CmdSpawner(Ghost, gameObject.transform.position, Quaternion.identity).transform.SetParent(GameObject.Find("Ghosts").transform, false);
            else Spawner(Ghost, gameObject.transform.position, Quaternion.identity).transform.SetParent(GameObject.Find("Ghosts").transform, false);
            */

            // 墓のオブジェクトを消す
            Destroy(gameObject, 0.02f);
        }
    }
    //対戦モード用スポナー
    [ServerCallback]
    private GameObject CmdSpawner(GameObject Obj, Vector3 Position, Quaternion Angle)
    {
        // オブジェクトを生成し、サーバーにスポーンさせる
        GameObject Objcln = Instantiate(Obj, Position, Angle);
        NetworkServer.Spawn(Objcln);
        return Objcln;
    }
    //オフラインモード用スポナー
    private GameObject Spawner(GameObject Obj, Vector3 Position, Quaternion Angle)
    {
        // オブジェクトを生成する
        GameObject Objcln = Instantiate(Obj, Position, Angle);
        return Objcln;
    }

    // コライダーに触れている間
    void OnTriggerStay(Collider other)
    {
        // 接触したオブジェクトのタグが"Child"のとき
        if (other.gameObject.tag == "Child")
        {
            Vector3 Childpos = other.gameObject.GetComponent<ChildrenController>().GetTarget();
            if(gameObject.transform.position == Childpos)
            {
                // 時間を足していく
                Breakgravetime += Time.deltaTime;

                // SEが重ならないように鳴らす
                if (!audioSource.isPlaying)
                {
                    // 墓を壊してる音を流す
                    //audioSource.PlayOneShot(Breakinggrave, 1.0f);
                }
            }
        }
        else if(other.gameObject.tag == "PlayerChild")
        {
            // 時間を足していく
            Breakgravetime += Time.deltaTime;

            // SEが重ならないように鳴らす
            if (!audioSource.isPlaying)
            {
                // 墓を壊してる音を流す
                //audioSource.PlayOneShot(Breakinggrave, 1.0f);
            }
        }

        // 墓の修理
        if(other.gameObject.tag == "Repair")
        {           
            Repairgravetime += Time.deltaTime;
            if(Repairgravetime < 5)
            {
                if (!audioSource.isPlaying) audioSource.PlayOneShot(Underrepair, 0.5f);
            }
            else if(Repairgravetime >= 5)
            {
                for(int i = 0; i < particleSystem.Length; i++)
                {
                    particleSystem[i].Play(true);
                }
                audioSource.Stop();
                audioSource.PlayOneShot(Repaircomplete, 0.5f);
                Breakgravetime = 0;
                Repairgravetime = 0;
                /*
                if(Onlineflg) GameObject.Find("Player(Network)(Clone)").GetComponent<NetworkPlayerMove>().RepairedGrave();
                else GameObject.Find("Player").GetComponent<PlayerController>().RepairedGrave();
                */
            }
        }
    }

    // コライダーから離れた時
    private void OnTriggerExit(Collider other)
    {
        // 接触したオブジェクトのタグが"Child"のとき
        if (other.gameObject.tag == "Child" || other.gameObject.tag == "PlayerChild")
        {
            audioSource.Stop();
        }

        if (other.gameObject.tag == "Player") Repairgravetime = 0;
    }

    public float GetBreakGraveTime()
    {
        return Breakgravetime;
    }
}
