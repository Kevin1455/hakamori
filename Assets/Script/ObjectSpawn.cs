﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Mirror;

public class ObjectSpawn : NetworkBehaviour
{
    [SerializeField]
    GameObject GhostObject;

    [SerializeField]
    GameObject GraveObject;

    [SerializeField]
    GameObject Matchbox;

    GameObject Objcln;

    private int Breakgravenum; // 壊されてはいけない墓の残り数
    private int Randomnum; // オブジェクトをランダムに出すために使用
    private readonly int[] Storagenum = new int[3]; // リストの中の数字を保管する器
    private readonly int Maxspawn = 3; // 出現できるマッチの最大数
    private bool Onlineflg;//対戦モードフラグ
    float Spawntime = 0;

    private List<GameObject> Gravestorage = new List<GameObject>();
    private List<GameObject> Targetgravelist = new List<GameObject>();
    int Targetgravenum;

    // 生成位置がかぶらないようにするリスト
    private List<int> Randomlist = new List<int>();

    // 墓を生成する座標と向き
    private readonly int[,] Gravepos = new int[12, 4]
    { //  x,  y,   z, 向き 
        {-25,  0,  22,  90},
        {-5,  0,  18, 180},
        {-20,  0,  5,  90},
        { 10,  0,  25, 180},
        { 25,  0,  7, 270},
        { 5,  0,  5, 270},
        { 10,  0, -5, 270},
        { 25,  0, -20, 270},
        { 5,  0, -25,   0},
        {-22,  0, -15,  90},
        {-10,  0, -27,   0},
        {-7,  0, -10,   0}
    };

    /*
    // マッチ箱を生成する座標
    private readonly float[,] Matchpos = new float[13, 3]
    {
        {-55,  1.5f,  40},
        {-10,  1.5f,  50},
        {-30,  1.5f,   5},
        { 45,  1.5f,  25},
        { 30,  1.5f,  55},
        { 10,  1.5f,  20},
        { 60,  1.5f, -10},
        { 55,  1.5f, -55},
        { 10,  1.5f, -40},
        {  0,  1.5f,   0},
        {-50,  1.5f, -15},
        {-10,  1.5f, -30},
        {-60,  1.5f, -60}
    };
    */
    // Start is called before the first frame update
    void Start()
    {
        Onlineflg = SceneManager.GetActiveScene().name == "OnlineScene";

        // リストに0～11までの数字を追加
        for (int i = 0; i < 12; i++)
        {
            Randomlist.Add(i);
        }

        // 初期化
        for(int i = 0; i < Maxspawn; i++)
        {
            Storagenum[i] = 0;
        }

        Breakgravenum = 5; // 壊されてはいけない墓の残り数
        int Gravenum = 5; // 墓の総数(難易度によって変わる)
        
        // 墓の生成
        for (int i = 0; i < Gravenum; i++)
        {
            // 墓の座標をランダムに選ぶ
            Randomnum = Random.Range(0, Randomlist.Count - 1);

            // 墓の生成
            if(!Onlineflg)
            {
                GraveObject = (GameObject)Resources.Load("Prefab/gravestone_01");
            }

            if (Onlineflg)
                CmdSpawner(GraveObject, new Vector3(Gravepos[Randomlist[Randomnum], 0], Gravepos[Randomlist[Randomnum], 1], Gravepos[Randomlist[Randomnum], 2]), 
                Quaternion.AngleAxis(Gravepos[Randomlist[Randomnum], 3], new Vector3(0.0f, 1.0f, 0.0f)));
            else 
            {
                Spawner(GraveObject, new Vector3(Gravepos[Randomlist[Randomnum], 0], Gravepos[Randomlist[Randomnum], 1], Gravepos[Randomlist[Randomnum], 2]), 
                Quaternion.AngleAxis(Gravepos[Randomlist[Randomnum], 3], new Vector3(0.0f, 1.0f, 0.0f)));
            }

            //if (i < StageChange.Stagenum)
            //{
            //    // ゴーストの生成
            //    if (!Onlineflg) GhostObject = (GameObject)Resources.Load("Prefab/Ghost");
            //    if (Onlineflg) 
            //        CmdSpawner(GhostObject, new Vector3(Gravepos[Randomlist[Randomnum], 0], Gravepos[Randomlist[Randomnum], 1], Gravepos[Randomlist[Randomnum], 2]), 
            //        Quaternion.AngleAxis(Gravepos[Randomlist[Randomnum], 3], new Vector3(0.0f, 1.0f, 0.0f)));
            //    else 
            //        Spawner(GhostObject, new Vector3(Gravepos[Randomlist[Randomnum], 0], Gravepos[Randomlist[Randomnum], 1], Gravepos[Randomlist[Randomnum], 2]), 
            //        Quaternion.AngleAxis(Gravepos[Randomlist[Randomnum], 3], new Vector3(0.0f, 1.0f, 0.0f)));
            //}

            // 生成した座標の配列が選ばれないようにリストから外す
            Randomlist.RemoveAt(Randomnum);
        }

        // リストの中身をすべて消す
        Randomlist.Clear();
        /*
        // マッチの生成用にリストの中身を追加する
        for(int i = 1; i < 14; i++)
        {
            Randomlist.Add(i);
        }
        */
        // 子供が逃げるときに追う用のオブジェクト生成
        //var parent = new GameObject ("EscapeObjects");
        //for(int i = -60; i <= 60; i += 20)
        //{
        //    for(int j = -60; j <= 60; j += 20)
        //    {
        //        var Escapepoint = new GameObject();
        //        Escapepoint.name = "EscapeObject";
        //        Escapepoint.tag = "Escape";
        //        Escapepoint.layer = 11;
        //        Escapepoint.AddComponent<SphereCollider>().isTrigger = true;
        //        Escapepoint.GetComponent<SphereCollider>().radius = 5.0f;
        //        Escapepoint.transform.parent = parent.transform;
        //        Escapepoint.transform.position = new Vector3(i, 0.06f, j);
        //    }
        //}

        Targetgravenum = 5;
        if(Targetgravenum == 0) Targetgravenum++;
        if (Onlineflg) AddTargetGrave();
        else if (Targetgravelist.Count != Targetgravenum)
        {
            for (int i = Targetgravelist.Count; i < Targetgravenum; i++)
            {
                int Rndnum = Random.Range(0, Gravestorage.Count);

                Targetgravelist.Add(Gravestorage[Rndnum]);
                Gravestorage.RemoveAt(Rndnum);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        Spawntime += Time.deltaTime;

        /*
        // 30秒おきにマッチを生成する
        if(Spawntime >= 30)
        {
            MatchSpawn();
            Spawntime = 0;
        }
        */
    }

    [ServerCallback]
    private void AddTargetGrave()
    {
        for (int i = Targetgravelist.Count; i < Targetgravenum; i++)
        {
            int Rndnum = Random.Range(0, Gravestorage.Count);

            Targetgravelist.Add(Gravestorage[Rndnum]);
            Gravestorage.RemoveAt(Rndnum);
        }
    }

    //対戦モード用スポナー
    [ServerCallback]
    private void CmdSpawner(GameObject Obj, Vector3 Position, Quaternion Angle)
    {
        Objcln = Instantiate(Obj, Position, Angle);
        if (Obj.name == "gravestone_01(Network)") Gravestorage.Add(Objcln);
        if (Obj.name == "Ghost(Network)") Objcln.transform.SetParent(GameObject.Find("Ghosts").transform, false);
        NetworkServer.Spawn(Objcln);
    }

    //オフラインモード用スポナー
    private void Spawner(GameObject Obj, Vector3 Position, Quaternion Angle)
    {
        Objcln = Instantiate(Obj, Position, Angle);
        if (Obj.name == "gravestone_01")
        {
            Objcln.transform.SetParent(GameObject.Find("GraveSpawn").transform, false);
            Gravestorage.Add(Objcln);
        }
        else if (Obj.name == "Ghost") Objcln.transform.SetParent(GameObject.Find("Ghosts").transform, false);
    }

    public Transform GetTargetGrave()
    {
        int Rndnum = Random.Range(0, Targetgravelist.Count);
        return Targetgravelist[Rndnum].transform;
    }

    [ServerCallback]
    public Transform GetOnlineTargetGrave()
    {
        int Rndnum = Random.Range(0, Targetgravelist.Count);
        return Targetgravelist[Rndnum].transform;
    }
    
    // 墓が壊された数を管理する関数
    public void BreakGrave(GameObject Obj)
    {
        // 墓が壊されたら数を減らす
        Breakgravenum--;

        if (Onlineflg) RemoveTargetGrave(Obj);
        else Targetgravelist.Remove(Obj);

        // 0になったらゲームオーバー
        if (Breakgravenum == 0)
        {
            if (SceneManager.GetActiveScene().name == "TutorialScene") SceneManager.LoadScene("TitleScene");
            else if (SceneManager.GetActiveScene().name != "OnlineScene")
            {
                SceneManager.sceneLoaded += ChildEndLoaded;
                SceneManager.LoadScene("GameOverScene");
            } 
            else
            {
                GameObject.Find("UICamera/Canvas").GetComponent<UIController>().SetGameOvar(true);
            }
        }
    }

    [ServerCallback]
    private void RemoveTargetGrave(GameObject Obj)
    {
        Targetgravelist.Remove(Obj);
    }

    // UI表示のため、壊されてはいけない墓の残りの数を渡す
    public int GetBreakGraveNum()
    {
        return Breakgravenum;
    }
    /*
    private void MatchSpawn()
    {
        // ランダムにスポーンする座標を決める
        Randomnum = Random.Range(0, Randomlist.Count);
        
        for (int i = 0; i < Maxspawn; i++)
        {
            // ストレージの中に空き(0)がある場合
            if (Storagenum[i] == 0)
            {
                // ストレージにランダムで選んだリストの値を入れる
                Storagenum[i] = Randomlist[Randomnum];

                // マッチのオブジェクトを用意する
                if(!Onlineflg) Matchbox = (GameObject)Resources.Load("Prefab/MatchBox");

                // マッチを生成する
                if (Onlineflg) CmdSpawner(Matchbox, new Vector3(Matchpos[Randomlist[Randomnum] - 1, 0], Matchpos[Randomlist[Randomnum] - 1, 1], Matchpos[Randomlist[Randomnum] - 1, 2]), Quaternion.identity);
                else Spawner(Matchbox, new Vector3(Matchpos[Randomlist[Randomnum] - 1, 0], Matchpos[Randomlist[Randomnum] - 1, 1], Matchpos[Randomlist[Randomnum] - 1, 2]), Quaternion.identity);

                // リストからRandomnum番目の値を外す
                Randomlist.RemoveAt(Randomnum);
                break;
            }
        }
    }
    */
    /*
    // スポーンする場所をリストから選ばれるように戻す関数
    public void SpawnPlace(Transform transform)
    {
        for(int i = 0; i < Maxspawn; i++)
        {
            if (Storagenum[i] == 0)
            {
                continue;
            }
            // transformがストレージの中にあるポジションと一緒だったら
            if (Matchpos[Storagenum[i] - 1, 0] == transform.position.x && Matchpos[Storagenum[i] - 1, 2] == transform.position.z)
            {
                // リストに添え字を入れる
                Randomlist.Add(Storagenum[i]);

                // ストレージの中を空(0)にする
                Storagenum[i] = 0;

                break;
            }
        }
    }
    */
    public bool GetOnlineFlg()
    {
        return SceneManager.GetActiveScene().name == "OnlineScene";
    }

    private void ChildEndLoaded(Scene next, LoadSceneMode mode)
    {
        var GameOverManager
            = GameObject.FindGameObjectWithTag("GOManager").GetComponent<GameOverManager>();

        GameOverManager.Diverge = 2;

        SceneManager.sceneLoaded -= ChildEndLoaded;
    }
}
