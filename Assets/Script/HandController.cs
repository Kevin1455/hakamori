﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.SceneManagement;

public class HandController : MonoBehaviour
{
    Animator animator;//アニメーター
    GameObject Player;//プレイヤー
    GameObject Handcol;//手の当たり判定
    PlayerController Plycon;//プレイヤーコントローラスクリプト
    float Handcooltime;//クールタイム

    private AnimatorStateInfo stateinfo;
    private float frame;

    public static bool Plyscene;//プレイシーンかどうか
    private GameObject Skychange;//世界の切り替え
    private bool World;//世界がどちらか

    // Start is called before the first frame update
    void Start()
    {
        if (SceneManager.GetActiveScene().name == "TitleScene")
        {
            Plyscene = false;
        }
        else Plyscene = true;

        Player = GameObject.Find("Player");//プレイヤーオブジェクトを格納
        Handcol = GameObject.Find("HandCollider");//手の当たり判定オブジェクトを格納
        animator = gameObject.GetComponent<Animator>();//アニメーターを取得
        Plycon = Player.GetComponent<PlayerController>();//プレイヤーコントローラを取得
        Handcol.SetActive(false);//手の当たり判定を無効
        Handcooltime = 0;//初期値0
        
        if(Plyscene) Skychange = GameObject.Find("SkyboxChange");

    }

    // Update is called once per frame
    void Update()
    {
        if (Plyscene)
        {
            World = Skychange.GetComponent<SkyboxChange>().SkyChangeFlag();//世界がどちらか

            if (World)
            {
                //RTが押されたら
                if (Input.GetButtonDown("Fire3") && !Plycon.GetHandFlag())
                {
                    animator.SetBool("Handanm", true);//アニメーション再生

                    Handcol.SetActive(true);//手の当たり判定を有効
                }
                if (Handcooltime < 1.0f && animator.GetBool("Handanm")) Handcooltime += Time.deltaTime;//クールタイム
                else if (Handcooltime >= 1.0f && animator.GetBool("Handanm"))
                {
                    animator.SetBool("Handanm", false);//アニメーション再生
                    Handcooltime = 0;//クールタイムのリセット
                    Plycon.SetHandFlg(false);//プレイヤーコントローラのHandflgをfalseに
                }
            }
        }
        else
        {
            //RTが押されたら
            if (Input.GetButtonDown("Fire3") && !Plycon.GetHandFlag())
            {
                animator.SetBool("Handanm", true);//アニメーション再生

                Handcol.SetActive(true);//手の当たり判定を有効
            }
            if (Handcooltime < 1.0f && animator.GetBool("Handanm")) Handcooltime += Time.deltaTime;//クールタイム
            else if (Handcooltime >= 1.0f && animator.GetBool("Handanm"))
            {
                animator.SetBool("Handanm", false);//アニメーション再生
                Handcooltime = 0;//クールタイムのリセット
                Plycon.SetHandFlg(false);//プレイヤーコントローラのHandflgをfalseに
            }
        }
    }

    //アニメーション再生終了後に呼び出される
    public void HandActiveFalse()
    {
        Handcol.SetActive(false);//手の当たり判定を無効
    }

    public void HandAnimation(bool Flg)
    {
        //animator.SetBool("Handanm", Flg);

        animator.Rebind();

        Handcooltime = 0;

        //gameObject.transform.localPosition = new Vector3(0.15f, -0.86f, 1.75f);

        Handcol.SetActive(false);//手の当たり判定を無効
    }
}
