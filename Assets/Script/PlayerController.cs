﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public static bool Plyscene;//プレイシーンかどうか

    AudioSource audioSource;
    AudioSource FireAudio;//ロウソクの火のオーディオソース
    AudioSource CrossAudio;//十字架のオーディオソース
    [SerializeField]
    AudioClip FireSE;//ロウソク使用時のSE
    public AudioClip Usecross;//十字架使用中のSE
    public AudioClip himei;//十字架使用中のSE
    private CharacterController charaCon;//キャラクターコンポーネント用の変数
    public float Speed = 5.0f; //移動速度（Public＝インスペクタで調整可能）
    public float kaitenSpeed = 1200.0f;//プレイヤーの回転速度（Public＝インスペクタで調整可能）
    private GameObject[] MatchNumImage;//マッチ残数UI
    GameObject MatchNumcln;//マッチ残数UIのクローン
    GameObject Candlefire;//ロウソクの火
    GameObject Matchandcandle;//マッチとロウソク
    GameObject Match;//マッチ
    GameObject Cross;//十字架
    GameObject Rockandtube;//石とチューブ
    GameObject Rock;//石オブジェクト
    GameObject Hand;//手
    GameObject RepairCollider;//墓修復アイテム
    GameObject KillerCollider;//ゴースト消滅アイテム
    GameObject Canvas;//キャンバス
    GameObject smallLight;//手のライトのあかり
    GameObject Light;//手のライト

    //private int matchnum;//マッチの残り本数
    //private int Rocknum;//石の所持数//3なら墓を1つ修復できる
    private int oldmatchnum;//1フレーム前のマッチ残数
    private int Havingitem;//手元に表示されているアイテム
    private int Haditem;//1フレーム前に持ってたアイテム
    private float FireTime;//火の制限時間
    //private bool Fireflg;//火がついているか
    private bool Handflg;//手が表示されているか
    private bool Usingitem;//アイテム使用中のフラグ
    private bool Usedkiller;//ゴースト消滅アイテム使用後trueに
    private bool Usedrepair;//墓修復アイテム使用後trueに
    //UIにflgが必要なためpublicに
    public bool Lightflg;//LightOnOFF
    //レンジ
    private GameObject LightRenge;//プレイヤーのレンジオンオフを格納するオブジェクト
    private GameObject Rng;//プレイヤーのレンジ

    private Vector3 movePower = Vector3.zero;    // キャラクター移動量（未使用）
    private const float gravityPower = 9.8f;         // キャラクター重力（未使用）
    
    public AudioClip LightOnOff;
    public AudioClip sound1;
    AudioSource walksound;//オーディオ所得するため

    private GameObject Skychange;//世界の切り替え
    private GameObject UICanvas;//世界の切り替え
    private GameObject Alart;
    private bool World;//世界がどちらか
    private bool LightButtery;//ライトのバッテリーがあるかどうか
    private bool Alartflg;

    private GameObject Childcatch;
    private Animator Catch;
    private AnimatorStateInfo Catchstateinfo;
    private bool frame;

    private GameObject Maincamera;
    private bool Ghostgameoverflg;
    private Transform Ghostpos;
    private float Angle;
    private float Angledif;

    private void Awake()
    {
        Maincamera = GameObject.Find("Main Camera");
    }

    // Start is called before the first frame update
    void Start()
    {
        //オーディオコンポーネントの所得
        walksound = GetComponent<AudioSource>();
        //タイトル画面でないならtrueに
        if (SceneManager.GetActiveScene().name == "TitleScene")
        {
            Plyscene = false;
        }
        else  Plyscene = true;

        charaCon = GetComponent<CharacterController>(); // キャラクターコントローラーのコンポーネントを参照する 
        //matchnum = 0;//マッチの初期値
        //Rocknum = 3;
        Ghostgameoverflg = false;

        //プレイ画面でのみ実行
        if (Plyscene)
        {
            //オブジェクトを見つけて格納する
            Hand = GameObject.Find("Hand");
            LightRenge = GameObject.Find("Player/LightRenge");//視界の当たり判定状態の格納
            Rng = GameObject.Find("Player/LightRenge/Range0");//視界の当たり判定格納
            Rng.SetActive(false);
            //Candlefire = GameObject.Find("Magic fire pro orange");
            //FireAudio = Candlefire.GetComponent<AudioSource>();
            //FireSE = (AudioClip)Resources.Load("Sound/matchfire");
            //MatchNumImage = new GameObject[] { (GameObject)Resources.Load("Prefab/MatchNum1"),
            //(GameObject)Resources.Load("Prefab/MatchNum2"), (GameObject)Resources.Load("Prefab/MatchNum3")};
            //Canvas = GameObject.Find("MatchCanvas");
            //Matchandcandle = GameObject.Find("MatchandCandle");
            //Match = GameObject.Find("MyMatchBox");
            //Cross = GameObject.Find("Cross");
            //CrossAudio = Cross.GetComponent<AudioSource>();
            //Rockandtube = GameObject.Find("RockandTube");
            //Rock = GameObject.Find("HaveRock_11");
            //RepairCollider = GameObject.Find("Repair");
            //KillerCollider = GameObject.Find("Cross/Killer");
            //Havingitem = 0;
            //Usingitem = false;
            //Usedrepair = false;

            //Cross.SetActive(false);
            //Rockandtube.SetActive(false);

            //Candlefire.SetActive(false);//ロウソクの炎消しておく
            //RepairCollider.SetActive(false);//修復アイテム非表示
            //KillerCollider.SetActive(false);//ゴースト消滅アイテム非表示
            //追加ライト
            Lightflg = false;
            Light = GameObject.Find("Flashlight");
            smallLight = GameObject.Find("lights");
            Light.SetActive(false);//ライト使用不可
            smallLight.SetActive(false);
            Alart = GameObject.Find("alart");
            Alart.SetActive(false);
            Alartflg = false;

            // Fireflg = false;//初期値false
            Handflg = false;//初期値false
            Skychange = GameObject.Find("SkyboxChange");
            UICanvas = GameObject.Find("UICamera/Canvas");

            Childcatch = GameObject.Find("child_douka");
            Catch = Childcatch.GetComponentInChildren<Animator>();
            Childcatch.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {
        /*//ロウソクの火がついている時間
        if (Fireflg)FireTime +=Time.deltaTime;
        //RTが押されたら
        */

        if (Input.GetButtonDown("Fire3") && !Handflg && World && !Ghostgameoverflg)
        {
            Handflg = true;//Handflgをtrue
        }

        if(Plyscene && !Ghostgameoverflg)
        {
            if(frame != World) Handflg = false;

            frame = World;

            World = Skychange.GetComponent<SkyboxChange>().SkyChangeFlag();//世界がどちらか
            LightButtery = UICanvas.GetComponent<UIController>().LightButteryflg();
            if (!World)
            {
                Alart.SetActive(false);
                if (!LightButtery)
                {
                    if (Input.GetButtonDown("Fire1"))
                    {
                        ///////////////////手の位置調整////////////////////
                        //Transform myTransform = Hand.transform;//手の位置調整
                        //// 座標を取得
                        //Vector3 pos = myTransform.position;
                        //pos.x = 0.15f;    //手の座標
                        //pos.y = -0.86f;   //手の座標
                        //pos.z = 1.75f;    //手の座標
                        //
                        //myTransform.position = pos;  // 座標を設定

                        Hand.GetComponent<HandController>().HandAnimation(false);

                        //////////////////////////////////////////////////

                        //Hand.SetActive(false);//手使用不可
                        Light.SetActive(true);//ライト使用
                        LightRenge.SetActive(true);//ライト使用
                    }
                    Lightflg = false;
                    Rng.SetActive(false);
                    smallLight.SetActive(false);//ライトオフ 
                }
                else
                {
                    if (Input.GetButtonDown("Fire1"))
                    {
                        ///////////////////手の位置調整////////////////////
                        //Transform myTransform = Hand.transform;//手の位置調整
                        //// 座標を取得
                        //Vector3 pos = myTransform.position;
                        //pos.x = 0.15f;    //手の位置調整
                        //pos.y = -0.86f;   //手の位置調整
                        //pos.z = 1.75f;    //手の位置調整
                        //
                        //myTransform.position = pos;  // 座標を設定

                        Hand.GetComponent<HandController>().HandAnimation(false);

                        ////////////////////////////////////////////////////

                        //Hand.SetActive(false);//手使用不可
                        Light.SetActive(true);//ライト使用
                        LightRenge.SetActive(true);//ライト使用
                    }
                    //ライトのオンオフ
                    if (Lightflg == false)
                    {
                        if (Input.GetButtonDown("Fire3"))
                        {
                            smallLight.SetActive(true);//ライトオフ 
                            Lightflg = true;
                            Rng.SetActive(true);
                            walksound.PlayOneShot(LightOnOff, 1.0f); // ライトをつけるSE
                            Alartflg = true;
                        }
                    }
                    else
                    {
                        if (Input.GetButtonDown("Fire3"))
                        {
                            smallLight = GameObject.Find("lights");
                            smallLight.SetActive(false);//ライトオフ 
                            Lightflg = false;
                            Rng.SetActive(false);
                            walksound.PlayOneShot(LightOnOff, 1.0f); // ライトを消すSE
                            Alart.SetActive(false);
                            Alartflg = false;
                        }
                    }
                }
                
            }
            else
            {
                Hand.SetActive(true);//手使用可
                Light.SetActive(false);//ライト使用不可
                LightRenge.SetActive(false);
                if (Alartflg) Alart.SetActive(true);
            }


            ////目を閉じている、目を開けた瞬間の移動速度の処理
            //if (Input.GetButton("Fire1") && Speed <= 60)
            //{
            //    Speed += 0.2f;
            //}
            //if (Input.GetButtonUp("Fire1"))
            //{
            //    Speed = 1.0f;
            //}
            ////通常の速度に戻す
            //else
            //{
            //    if(Speed < 7.0f)Speed += 0.1f;
            //}

            /*
            //アイテムの切り替え
            Haditem = Havingitem;
            if (Usedkiller || Usedrepair)
            {
                Havingitem = 0;
                Usedkiller = false;
                Usedrepair = false;
            }

            if (Input.GetAxis("JoyUD") >= 0.5f && !Usingitem) Havingitem = 0;
            if(Input.GetAxis("JoyLR") >= 0.5f && !Usingitem && Rockandtube != null) Havingitem = 1;
            if(Input.GetAxis("JoyLR") <= -0.5f && !Usingitem && Cross != null) Havingitem = 2;

            //アイテムの処理
            switch (Havingitem)
            {
                case 0:
                    UseMatch();
                    break;
                case 1:
                    UseRepair();
                    break;
                case 2:
                    UseKiller();
                    break;
                default:
                    Debug.Log("Error Item");
                    break;
            }

            if (Havingitem == 0)DisplayMatch();//手元のマッチ表示
            else if(Havingitem == 1)DisplayRepair();
            else if(Havingitem == 2)DisplayKiller();

            Debug.Log("Now Item Num = " + Havingitem);
            Debug.Log("Item using " + Usingitem);
            */
        }

        else if(Plyscene && Ghostgameoverflg)
        {
            if(Angledif <= 180)
            {
                if(Angle <= gameObject.transform.localEulerAngles.y) gameObject.transform.localEulerAngles = new Vector3(0, Angle, 0);
                else gameObject.transform.localEulerAngles += new Vector3(0, 0.05f, 0);
            }
            else if(Angledif > 180)
            {
                if (Angle >= gameObject.transform.localEulerAngles.y) gameObject.transform.localEulerAngles = new Vector3(0, Angle, 0);
                else gameObject.transform.localEulerAngles -= new Vector3(0, 0.05f, 0);
            }

            Camera.main.transform.localEulerAngles = gameObject.transform.localEulerAngles;
            //Quaternion q = Camera.main.transform.rotation;
            //this.gameObject.transform.rotation = q;
        }
        
        //浮いてしまった場合の落下処理
        if(this.gameObject.transform.position.y > 3)this.gameObject.transform.position 
                = new Vector3(gameObject.transform.position.x, 3.0f, gameObject.transform.position.z);

        else if (Plyscene == false)
        {
            if (Input.GetAxis("Vertical") != 0|| Input.GetAxis("Horizontal") != 0)
            {
                if (!walksound.isPlaying)
                {
                    //音(sound1)を鳴らす
                    walksound.PlayOneShot(sound1);
                }
            }
            else
            {
                AudioSource source = gameObject.GetComponent<AudioSource>();
                source.Stop();
            }
        }

        if (Catch)
        {
            Catchstateinfo = Catch.GetCurrentAnimatorStateInfo(0);

            if(Catchstateinfo.normalizedTime >= 1.0f)
            {
                Childcatch.SetActive(false);

                Handflg = false;
            }
        }

        if (!Ghostgameoverflg)
        {
            //移動処理
            var cameraForward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;//カメラが追従するための動作

            Vector3 direction = cameraForward * Input.GetAxis("Vertical") + Camera.main.transform.right * Input.GetAxis("Horizontal");//キーやスティックの入力

            CameraMove();
            Move(direction);//移動する動作の処理を実行する
        }
    }

    private void CameraMove()
    {
        Vector3 v = gameObject.transform.position;
        v.y -= 0.4f;
        Camera.main.transform.position = v;

        Quaternion q = Camera.main.transform.rotation;
        this.gameObject.transform.rotation = q;
    }

    private void UseMatch()
    {
        /*
        //ロウソクに火をつける処理
        if (Input.GetButtonDown("Fire2") && !Handflg && matchnum > 0 && !Fireflg)
        {
            Candlefire.SetActive(true);
            //SE再生
            FireAudio.PlayOneShot(FireSE, 0.5f);
            Fireflg = true;
            Usingitem = true;
            matchnum--;
        }
        //10秒後に消える
        else if (FireTime >= 10)
        {
            Candlefire.SetActive(false);
            Fireflg = false;
            Usingitem = false;
            FireTime = 0;
        }
        */
    }

    //墓修復アイテムの使用
    private void UseRepair()
    {
        /*
        if(Input.GetButton("Fire2") && !RepairCollider.activeSelf && Rocknum >= 1)
        {
            RepairCollider.SetActive(true);
            Usingitem = true;
        }
        else if(Input.GetButtonUp("Fire2"))
        {
            RepairCollider.SetActive(false);
            Usingitem = false;
        }
        */
    }

    //ゴースト消滅アイテムの使用
    private void UseKiller()
    {
        /*
        if(Input.GetButtonUp("Fire2") && Usingitem && KillerCollider.activeSelf)
        {
            CrossAudio.Stop();
            KillerCollider.SetActive(false);
            Usingitem = false;
        }
        else if(Input.GetButtonDown("Fire2") && !KillerCollider.activeSelf)
        {
            KillerCollider.SetActive(true);
            Usingitem = true;
        }

        if (Input.GetButton("Fire2"))
        {
            if (!CrossAudio.isPlaying) CrossAudio.PlayOneShot(Usecross, 0.5f);
        }
        */
    }
    /*
    public void SetUsedKiller()
    {
        Usedkiller = true;
        Usingitem = false;
        Destroy(Cross);
        Cross = null;
    }

    //墓修復アイテム(石)の所持数を減らす
    //GraveControllerから呼び出す
    public void RepairedGrave()
    {
        RepairCollider.SetActive(false);
        Rocknum--;
        if (Rocknum <= 0)
        {
            Usedrepair = true;
            Destroy(Rockandtube);
            Rockandtube = null;
        }
    }

    private void DisplayMatch()
    {
        if(Havingitem != Haditem)
        {
            Matchandcandle.SetActive(true);
            if(Haditem == 1 && Rockandtube != null) Rockandtube.SetActive(false);
            if(Haditem == 2 && Cross != null) Cross.SetActive(false);
        }

        //マッチの残りの本数で見た目を変える
        //0なら箱ごと消す
        if (matchnum <= 0 || Handflg)
        {
            if(Match.activeSelf)Match.SetActive(false);
        }
        //全部表示する
        else
        {
            Match.SetActive(true);
        }

        //マッチ残数UIの表示
        if (oldmatchnum != matchnum)
        {
            if (matchnum != 0)
            {
                Destroy(MatchNumcln, 0.1f);
                MatchNumcln = Instantiate(MatchNumImage[matchnum - 1], new Vector3(0, 0, 0), Quaternion.identity);
                MatchNumcln.transform.SetParent(Canvas.transform, false);
            }
            else
            {
                Destroy(MatchNumcln, 0.1f);
            }
            oldmatchnum = matchnum;
        }
    }

    //墓修復アイテムの表示
    private void DisplayRepair()
    {
        if (Havingitem != Haditem)
        {
            Rockandtube.SetActive(true);
            if(Haditem == 0) Matchandcandle.SetActive(false);
            if(Haditem == 2) Cross.SetActive(false);
        }
        if(Rocknum <= 0 || Handflg) Rock.SetActive(false);
        else Rock.SetActive(true);
    }

    //ゴースト消滅アイテムの表示
    private void DisplayKiller()
    {
        if (Havingitem != Haditem)
        {
            Cross.SetActive(true);
            if(Haditem == 0) Matchandcandle.SetActive(false);
            if(Haditem == 1) Rockandtube.SetActive(false);
        }
        if (Handflg) Cross.SetActive(false);
        else Cross.SetActive(true);
    }
    */
    //移動する動作の処理
    void Move(Vector3 Moverange)
    {
        charaCon.Move(Moverange * Time.deltaTime * Speed);   // プレイヤーの移動距離は時間×移動スピードの値
    }
    /*
    //マッチを拾ったら残数を1増やす
    public void MatchCounter()
    {
        if(matchnum < 3) matchnum++;
    }
    
    //石を拾ったら残数を1増やす
    public void RockCounter()
    {
        Rocknum++;
    }
    */
    //手フラグのSet関数
    public void SetHandFlg(bool Flg)
    {
        Handflg = Flg;
    }
    //ライトのオンオフを渡す関数
    public bool LightSwitchflg()
    {
        return Lightflg;
    }

    public bool GetHandFlag()
    {
        return Handflg;
    }

    public void SetCatchFlg(bool Flg)
    {
        Childcatch.SetActive(Flg);
    }

    // お化けに当たってゲームオーバーになる時
    public void SetGhostPosition(Transform pos)
    {
        //if(World) Maincamera.GetComponent<CameraController>().GameoverSky();
        Ghostpos = pos;
        //Ghostgameoverflg = true;
        float tanx = Ghostpos.position.x - gameObject.transform.position.x;
        float tanz = Ghostpos.position.z - gameObject.transform.position.z;
        //float tanx = gameObject.transform.position.x - Ghostpos.position.x;
        //float tanz = gameObject.transform.position.z - Ghostpos.position.z;
        Angle = Mathf.Atan2(tanz, tanx) * Mathf.Rad2Deg;

        //if(Angle < 0)  Angle += 360;
        //Angle += 270;

        Angledif = Angle - gameObject.transform.localEulerAngles.y;
        if(Angledif < 0) Angledif += 360;

        Debug.Log(Angle);
        //Debug.Log(gameObject.transform.localEulerAngles.y);
    }

    public bool GetRotation()
    {
        if(Angle == gameObject.transform.localEulerAngles.y) return true;
        else return false;
    }
}
