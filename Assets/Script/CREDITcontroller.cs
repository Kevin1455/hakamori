﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CREDITcontroller : MonoBehaviour
{
    public CameraController CameraCRE;//CameraEffectのコンポーネント
    public PlayerController PlayCRE;//CameraEffectのコンポーネント
    GameObject CREDIT; //クレジット
    GameObject CREDITBack; //クレジット
    GameObject player;
    private bool CREflg = false;
    public static int SensiNum = 2;
    private GameObject Menu;
     //Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        PlayCRE = player.GetComponent<PlayerController>();//コンポーネントを取得
        CameraCRE = Camera.main.GetComponent<CameraController>();//コンポーネントを取得
        CREDIT = GameObject.Find("CREDITImage");
        CREDIT.SetActive(false);
        CREDITBack = GameObject.Find("CREDITBack");
        CREDITBack.SetActive(false);
        Menu = GameObject.Find("My Home/WallandFloor/FrowardWall/Titlelogo/Canvas");
    }

    // Update is called once per frame
    void Update()
    {
        if(CREflg == true && Input.GetButton("Fire1"))
        {
            CREflg = false;
            CREDIT.SetActive(false);
            CREDITBack.SetActive(false);
            PlayCRE.enabled = true;
            CameraCRE.enabled = true;
            GameObject.Find("Hand").GetComponent<HandController>().enabled = true;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Hand" && !Menu.GetComponent<TitleScene>().MenuFlgflg())
        {
            Debug.Log("A");
            CREDIT.SetActive(true);
            CREDITBack.SetActive(true);
            CREflg = true;
            PlayCRE.enabled = false;
            CameraCRE.enabled = false;
            GameObject.Find("Hand").GetComponent<HandController>().enabled = false;
        }
    }
    //CREDITが表示されていることを渡す関数
    public bool CREDITflg()
    {
        return CREflg;
    }
}
