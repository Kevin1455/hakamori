﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeClose : MonoBehaviour
{
    private Animator Closeanim;

    // Start is called before the first frame update
    void Start()
    {
        Closeanim = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CloseAnimation(bool Flg)
    {
        Closeanim.SetBool("Eyeclose", Flg);
    }
}
