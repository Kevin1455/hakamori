﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraveHPController : MonoBehaviour
{
    private GraveController graveController; 
    private MeshRenderer r;
    private Color redcolor;
    private Color yellowcolor;
    private Color greencolor;
    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        graveController = GetComponentInParent<GraveController>();

        r = GetComponent<MeshRenderer>();

        // マテリアルの変更するパラメータを事前に知らせる
        r.material.EnableKeyword("_EMISSION");

        redcolor = new Color(255, 0, 0);
        yellowcolor = new Color(255, 255, 0);
        greencolor = new Color(0, 255, 0);
    }

    // Update is called once per frame
    void Update()
    {
        timer = graveController.GetBreakGraveTime();
        if(timer > 20.0f) Destroy(gameObject);
        else if(timer > 13.0f) r.material.SetColor("_EmissionColor", redcolor);
        else if(timer > 6.0f) r.material.SetColor("_EmissionColor", yellowcolor);
        else r.material.SetColor("_EmissionColor", greencolor);
    }
}
