﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyScene : MonoBehaviour
{
    private float x;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = true;
    }

    // Update is called once per frame
    void Update()
    {
        x -= 0.03f;
        this.gameObject.transform.rotation = Quaternion.Euler(x, 0.0f, 90.0f);
    }
}
