﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GraveHPCamera : MonoBehaviour
{
    private GameObject MainCamera;
    private new Camera camera;
    private bool Onlineflg;

    // Start is called before the first frame update
    void Start()
    {
        Onlineflg = SceneManager.GetActiveScene().name == "OnlineScene";
        if(Onlineflg) MainCamera = GameObject.Find("MainCamera");
        else MainCamera = GameObject.Find("Main Camera");
        camera = gameObject.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position =
            new Vector3(MainCamera.transform.position.x, 
                        MainCamera.transform.position.y - 350, 
                        MainCamera.transform.position.z);
        gameObject.transform.rotation = MainCamera.transform.rotation;

        if (Input.GetButton("Fire1")) camera.farClipPlane = 300;
        else camera.farClipPlane = 0.02f;
    }
}
