﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageChange : MonoBehaviour
{
    Animator animator;//アニメーター
    GameObject Cam;//メインカメラ
    float StageChangeTime;//ドアが開いてからステージ遷移するまでの時間
    public static int Stagenum;//PlaySceneに遷移した際の難易度

    public AudioClip sound1;
    AudioSource doorsound;//オーディオ所得するため
    bool doorflg = false;
    int doorcount = 0;//回数制御

    // Start is called before the first frame update
    void Start()
    {
        //オーディオコンポーネントの所得
        doorsound = GetComponent<AudioSource>();
        Cam = GameObject.Find("Main Camera");//メインカメラオブジェクトを格納
        StageChangeTime = 0;//初期値0
        animator = GetComponent<Animator>();//アニメーター取得
        Stagenum = 0;//遷移するステージの難易度
    }

    // Update is called once per frame
    void Update()
    {
        //ドアのアニメーションが取得されていたら
        if (animator != null)
        {
            //ドアのアニメーションが再生されていたら
            if(animator.GetBool("DoorSts"))
            {
                //演出
                if(!Cam.GetComponent<CameraEffect>().enabled && StageChangeTime >= 2.0f) Cam.GetComponent<CameraEffect>().enabled = true;
                if (StageChangeTime >= 2.0f) Cam.GetComponent<CameraEffect>().Closeeye();
                StageChangeTime += Time.deltaTime;
            }

            if(StageChangeTime >= 3.0f)
            {
                //このオブジェクトの名前を取得
                var Doorname = this.gameObject.name;

                
                //ステージ難易度設定
                if(Doorname == "TDoor")
                {
                    SceneManager.LoadScene("TutorialScene");return;
                }
                if(Doorname == "EDoor")Stagenum = 1;
                if(Doorname == "NDoor")Stagenum = 2;
                if(Doorname == "HDoor")Stagenum = 3;
                if(Doorname == "BDoor")
                {
                    Stagenum = 2;
                    SceneManager.LoadScene("OfflineScene");//対戦モード
                    return;
                }
                /*
                switch (Doorname)
                {
                    case "TDoor":
                        SceneManager.LoadScene("TutorialScene");
                        break;

                    case "EDoor":
                        Stagenum = 1;
                        SceneManager.LoadScene("PlayScene");//プレイ画面へ遷移
                        break;

                    case "NDoor":
                        Stagenum = 2;
                        SceneManager.LoadScene("PlayScene");//プレイ画面へ遷移
                        break;
                    case "HDoor":   
                        Stagenum = 3;
                        SceneManager.LoadScene("PlayScene");//プレイ画面へ遷移
                        break;
                    case "BDoor":
                        Stagenum = 2;
                        SceneManager.LoadScene("OfflineScene");//対戦モード
                        break;
                }*/

                SceneManager.LoadScene("PlayScene");//プレイ画面へ遷移
            }
        }

        if (doorflg == true)
        {
            if (!doorsound.isPlaying && doorcount == 0)
            {
                //音(sound1)を鳴らす
                doorsound.PlayOneShot(sound1);
                doorcount++;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Hand") 
        {
            GameObject Ply = GameObject.Find("Player");//プレイヤーオブジェクトを格納
            GameObject Menu = GameObject.Find("My Home/WallandFloor/FrowardWall/Titlelogo/Canvas");//プレイヤーオブジェクトを格納
            Ply.GetComponent<PlayerController>().enabled = false;//プレイヤーのスクリプトのenabledをfalse
            Cam.GetComponent<CameraController>().enabled = false;//メインカメラのスクリプトのenabledをfalse
            Menu.GetComponent<TitleScene>().enabled = false;//Menuを表示できないよう
            this.gameObject.GetComponent<BoxCollider>().enabled = false;//ドアの当たり判定を無効
            animator.SetBool("DoorSts", true);//ドアのアニメーションを再生
            doorflg = true;
        }
    }
}
