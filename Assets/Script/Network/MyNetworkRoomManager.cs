﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Mirror;

public class MyNetworkRoomManager : NetworkRoomManager
{
    [SerializeField] GameObject SpawnEnemyPrefab;
    int Membernum;

    public override GameObject OnRoomServerCreateGamePlayer(NetworkConnection conn, GameObject roomPlayer)
    {
        // ルームプレイヤーのネットIDを取得
        uint Netid = roomPlayer.GetComponent<NetworkIdentity>().netId;
        GameObject newgameObject = null;

        // ネットIDが1以外の場合、プレイヤーオブジェクトを子供に変える
        if (Netid != 1) newgameObject = SpawnEnemyPrefab;

        return newgameObject;
    }

    // 対戦終了時、RoomSceneに戻す処理
    public void FinishBattle()
    {
        ServerChangeScene(RoomScene);
    }



    /// <summary>
    /// Called just after GamePlayer object is instantiated and just before it replaces RoomPlayer object.
    /// This is the ideal point to pass any data like player name, credentials, tokens, colors, etc.
    /// into the GamePlayer object as it is about to enter the Online scene.
    /// </summary>
    /// <param name="roomPlayer"></param>
    /// <param name="gamePlayer"></param>
    /// <returns>true unless some code in here decides it needs to abort the replacement</returns>
    public override void OnRoomStopClient()
    {
        // Demonstrates how to get the Network Manager out of DontDestroyOnLoad when
        // going to the offline scene to avoid collision with the one that lives there.
        if (gameObject.scene.name == "DontDestroyOnLoad" && !string.IsNullOrEmpty(offlineScene) && SceneManager.GetActiveScene().path != offlineScene)
            SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetActiveScene());

        base.OnRoomStopClient();
    }

    public override void OnRoomStopServer()
    {
        // Demonstrates how to get the Network Manager out of DontDestroyOnLoad when
        // going to the offline scene to avoid collision with the one that lives there.
        if (gameObject.scene.name == "DontDestroyOnLoad" && !string.IsNullOrEmpty(offlineScene) && SceneManager.GetActiveScene().path != offlineScene)
            SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetActiveScene());

        base.OnRoomStopServer();
    }

    /*
        This code below is to demonstrate how to do a Start button that only appears for the Host player
        showStartButton is a local bool that's needed because OnRoomServerPlayersReady is only fired when
        all players are ready, but if a player cancels their ready state there's no callback to set it back to false
        Therefore, allPlayersReady is used in combination with showStartButton to show/hide the Start button correctly.
        Setting showStartButton false when the button is pressed hides it in the game scene since NetworkRoomManager
        is set as DontDestroyOnLoad = true.
    */

    bool showStartButton;

    public override void OnRoomServerPlayersReady()
    {
        // calling the base method calls ServerChangeScene as soon as all players are in Ready state.
#if UNITY_SERVER
            base.OnRoomServerPlayersReady();
#else
        showStartButton = true;
#endif
    }

    public override void OnGUI()
    {
        base.OnGUI();

        //半透明の矩形
        if (allPlayersReady && showStartButton && GUI.Button(new Rect(220, 500, 160, 40), "START GAME"))
        {
            // set to false to hide it in the game scene
            showStartButton = false;

            // ゲーム開始時のルーム内にいる人数を取得
            Membernum = roomSlots.Count;

            // GameplaySceneへ移動
            ServerChangeScene(GameplayScene);
        }

        if (NetworkServer.active && IsSceneActive(GameplayScene))
        {
            // GameplayScene中の人数とゲーム開始時の人数が異なった場合、RoomSceneに戻す
            if (Membernum != roomSlots.Count) ServerChangeScene(RoomScene);

            // 常にプレイ中の人数を更新する
            Membernum = roomSlots.Count;
        }
    }
}
