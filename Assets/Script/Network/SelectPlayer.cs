﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

//対戦モード用 UIに関する値と勝敗決定フラグを保持する
public class SelectPlayer : NetworkBehaviour
{
    public static int type = 2;

    private int Type;

    [SyncVar]
    private int GraveNum;//墓の残数
    [SyncVar]
    private Quaternion LongNeedle;//長針
    [SyncVar]
    private Quaternion ShortNeedle;//短針
    [SyncVar]
    private bool Npcspawnflg;//NPC子供の生成フラグ
    [SyncVar]
    private bool Clearflg;//クリアフラグ
    [SyncVar]
    private bool Gameovarflg;//ゲームオーバーフラグ
    
    bool Child;
    bool Player;
    private bool Scenechange;//シーンチェンジフラグ
    
    public void SetType(int Num) { Type = Num;}//ホストかクライアントかを判別

    public int GetPlyType() { return Type; }

    //クライアントがUIの値を同期する
    public void SetSomething(int Grv, Quaternion LAngle, Quaternion SAngle, bool Flg)
    {
        GraveNum = Grv;
        LongNeedle = LAngle;
        ShortNeedle = SAngle;
        Clearflg = Flg;
    }

    // ホスト側がUIの値をセット

    public void SetNpcSpawnFlg(bool Flg){ Npcspawnflg = Flg; }

    public bool GetNpcSpawnFlg() { return Npcspawnflg; }

    public void SetGameOvarFlg(bool Flg){ Gameovarflg = Flg;}

    public int GetGrvNum(){ return GraveNum; }

    public Quaternion GetLRotate(){ return LongNeedle; }

    public Quaternion GetSRotate(){ return ShortNeedle; }

    public bool GetClearFlg(){ return Clearflg; }

    public bool GetGameOvarFlg() { return Gameovarflg; }
    
    //勝敗決定
    [ClientCallback]
    public int VsResult(int Res)
    {
        int Result = 0;
        if(Res == 1) Result = 2;
        else if(Res == 2)Result = 1;

        return Result;
    }

    public void SetChild(bool Flg) { Child = Flg; }

    public void SetPlayer(bool Flg) { Player = Flg; }

    //シーンチェンジフラグを返す
    public bool SceneChangeFlg()
    {
        if (Child && Player)
        {
            return true;
        }
        return false;
    }
}
