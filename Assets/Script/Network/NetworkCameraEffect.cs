﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

[ExecuteInEditMode, ImageEffectAllowedInSceneView]
public class NetworkCameraEffect : MonoBehaviour
{
    private GameObject Ply;//プレイヤーオブジェクト
    private GameObject Rng;//視界の当たり判定オブジェクト
    private bool Startflg;//ゲームスタートフラグ

    [SerializeField, Range(0, 30)]
    private int _iteration = 0;//ぼかしの強さ
    // 4点をサンプリングして色を作るマテリアル
    [SerializeField]
    private Material _material;
    private float Limittime;//_iterationの値を+1するまでの時間
    private float Nowtime;//現在の経過時間
    private int isPlayer;//プレイヤーかどうか
    private RenderTexture[] _renderTextures = new RenderTexture[30];

    private void OnRenderImage(RenderTexture source, RenderTexture dest)
    {
        _material = (Material)Resources.Load("Prefab/Materials/Shader");//マテリアル取得
        var width = source.width;//画面の幅
        var height = source.height;//画面の高さ
        var currentSource = source;//画面の情報を格納

        var i = 0;
        RenderTexture currentDest = null;

        //ダウンサンプリング
        for (; i < _iteration; i++)
        {
            width /= 2;
            height /= 2;
            if (width < 2 || height < 2)
            {
                break;
            }
            currentDest = _renderTextures[i] = RenderTexture.GetTemporary(width, height, 0, source.format);

            //Blit時にマテリアルとパスを指定する
            Graphics.Blit(currentSource, currentDest, _material, 0);

            currentSource = currentDest;
        }

        //アップサンプリング
        for (i -= 2; i >= 0; i--)
        {
            currentDest = _renderTextures[i];

            //Blit時にマテリアルとパスを指定する
            Graphics.Blit(currentSource, currentDest, _material, 1);

            _renderTextures[i] = null;
            RenderTexture.ReleaseTemporary(currentSource);
            currentSource = currentDest;
        }

        // 最後にdestにBlit
        Graphics.Blit(currentSource, dest, _material, 1);
        if (_iteration > 0)
        {
            RenderTexture.ReleaseTemporary(currentSource);
        }
    }

    void Start()
    {
        Startflg = true;//初期値true
        _iteration = 0;//初期値0
        Limittime = 10;//10秒
        Nowtime = 0;//初期値0
        Ply = null;
        Rng = null;
        isPlayer = 2;
    }

    void Update()
    {
        if (Startflg)
        {
            //開始演出
            if (_iteration == 0) _iteration = 11;
            StartCameraEffect();
        }
        else if(isPlayer == 0)
        {

            //10秒ごとに1づつぼかしていく
            Nowtime += Time.deltaTime;
            if (Nowtime > Limittime)
            {
                _iteration++;
                Nowtime = 0;
            }

            //視界の当たり判定に関する処理
            if (_iteration == 1)
            {
                Rng.SetActive(true);
                Rng = GameObject.Find("Player(Network)(Clone)/Range0/Range1/Range2/Range3/Range4");
                Rng.SetActive(false);
            }
            else if (_iteration == 2)
            {
                Rng.SetActive(true);
                Rng = GameObject.Find("Player(Network)(Clone)/Range0/Range1/Range2/Range3");
                Rng.SetActive(false);
            }
            else if (_iteration == 3)
            {
                Rng.SetActive(true);
                Rng = GameObject.Find("Player(Network)(Clone)/Range0/Range1/Range2");
                Rng.SetActive(false);
            }
            else if (_iteration == 4)
            {
                Rng.SetActive(true);
                Rng = GameObject.Find("Player(Network)(Clone)/Range0/Range1");
                Rng.SetActive(false);
            }
            else if (_iteration > 4)
            {
                Rng.SetActive(true);
                Rng = GameObject.Find("Player(Network)(Clone)/Range0");
                Rng.SetActive(false);
            }
        }
    }

    private void FixedUpdate()
    {        
        if(Ply == null) Ply = GameObject.Find("Player(Network)(Clone)");//墓守オブジェクトを取得
        if(Rng == null) Rng = GameObject.Find("Player(Network)(Clone)/Range0").gameObject;//視界の当たり判定格納
        if(Ply != null)//プレイヤーの種類をセット
        {
            if (GameObject.Find("UICamera/Canvas").GetComponent<SelectPlayer>().GetPlyType() == 0) isPlayer = 0;
            else isPlayer = 1;
        }
    }

    //ぼかしをリセット
    public void Set_iteration()
    {
        _iteration = 0;
        if (Rng != null) Rng.SetActive(true);
    }
    //瞬きを行う関数
    public void Closeeye()
    {
        if (_iteration < 11) _iteration += 2;
    }

    //開始時演出関数
    public void StartCameraEffect()
    {
        if (_iteration > 0) _iteration -= 1;

        if (_iteration <= 0)
        {
            Startflg = false;
            this.enabled = false;
        }
    }

    public int GetisPlayer()
    {
        return isPlayer;
    }
}
