﻿using System.ComponentModel;
using UnityEngine;
using System;
using System.Net;
using UnityEngine.SceneManagement;

namespace Mirror
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Network/NetworkManagerHUD")]
    [RequireComponent(typeof(NetworkManager))]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [HelpURL("https://mirror-networking.com/docs/Components/NetworkManagerHUD.html")]
    public class MyNetworkManagerHUD : MonoBehaviour
    {
        NetworkManager manager;

        /// <summary>
        /// Whether to show the default control HUD at runtime.
        /// </summary>
        public bool showGUI = true;

        /// <summary>
        /// The horizontal offset in pixels to draw the HUD runtime GUI at.
        /// </summary>
        public int offsetX;

        /// <summary>
        /// The vertical offset in pixels to draw the HUD runtime GUI at.
        /// </summary>
        public int offsetY;

        // ホストかクライアントかを分けるフラグ
        bool HostPC = false;

        void Awake()
        {
            // NetworkManagerの取得
            manager = GetComponent<NetworkManager>();
        }

        void Start()
        {
            // テキストに記入されているIPアドレスを取得
            GetIPAddress getIPAddress = new GetIPAddress();
            string IPAddresstext = getIPAddress.GetIPAddressText();

            // 使っているパソコンのIPアドレスを取得
            string hostname = Dns.GetHostName();
            IPAddress[] adrList = Dns.GetHostAddresses(hostname);


            foreach (IPAddress address in adrList)
            {
                // IPアドレスが一致していれば、ホストになる
                if (IPAddresstext == address.ToString())
                {
                    HostPC = true;
                    break;
                }
            }

            // テキストのIPアドレスをNetworkManagerに送る
            manager.networkAddress = IPAddresstext;

            // それぞれでNetworkManagerの処理を行う
            if (HostPC) manager.StartHost();
            else manager.StartClient();
        }

        void OnGUI()
        {
            // LobbyScene以外ではGUIを非表示にする
            if (SceneManager.GetActiveScene().name == "LobbyScene") showGUI = true;
            else showGUI = false;

            if (!showGUI)
                return;

            //Stop Host Button
            GUILayout.BeginArea(new Rect(10 + offsetX, 40 + offsetY, 215, 9999));
            if (!NetworkClient.isConnected && !NetworkServer.active)
            {
                StartButtons();
            }
            else
            {
                StatusLabels();
            }

            // client ready
            if (NetworkClient.isConnected && !ClientScene.ready)
            {
                if (GUILayout.Button("Client Ready"))
                {
                    ClientScene.Ready(NetworkClient.connection);

                    if (ClientScene.localPlayer == null)
                    {
                        ClientScene.AddPlayer(NetworkClient.connection);
                    }
                }
            }

            StopButtons();

            GUILayout.EndArea();
        }

        void StartButtons()
        {
            if (NetworkClient.active)
            {
                 // Connecting
                 GUILayout.Label("Connecting to " + manager.networkAddress + "..");
                 if (GUILayout.Button("Cancel Connection Attempt"))
                 {
                     manager.StopClient();
                 }
            }
        }

        void StatusLabels()
        {
            // server / client status message
            if (NetworkServer.active)
            {
                GUILayout.Label("Server: active. Transport: " + Transport.activeTransport);
            }
            if (NetworkClient.isConnected)
            {
                GUILayout.Label("Client: address=" + manager.networkAddress);
            }
        }

        void StopButtons()
        {
            // stop host if host mode
            if (NetworkServer.active && NetworkClient.isConnected)
            {
                if (GUILayout.Button("Stop Host"))
                {
                    manager.StopHost();
                }
            }
            // stop client if client-only
            else if (NetworkClient.isConnected)
            {
                if (GUILayout.Button("Stop Client"))
                {
                    manager.StopClient();
                }
            }
            // stop server if server-only
            else if (NetworkServer.active)
            {
                if (GUILayout.Button("Stop Server"))
                {
                    manager.StopServer();
                }
            }
        }
    }
}