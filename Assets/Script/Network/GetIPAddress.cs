﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using System;

public class GetIPAddress : MonoBehaviour
{
    private string readTxt;
    private string path = "IPAddress.txt";
    private TextAsset IPAddresstext;
    float Displaytime;//表示時間
    private GameObject GHide1;//タイトルロゴ
    private GameObject GHide2;//操作説明
    private GameObject GHide3;//操作説明
    private GameObject GHide4;//操作説明

    void Start()
    {
        GHide1 = GameObject.Find("GhostHide1");//タイトルロゴオブジェクトを格納
        GHide2 = GameObject.Find("GhostHide2");//操作説明オブジェクトを格納
        GHide3 = GameObject.Find("GhostHide3");//タイトルロゴオブジェクトを格納
        GHide4 = GameObject.Find("GhostHide4");//操作説明オブジェクトを格納
    }

    void Update()
    {
        Displaytime += Time.deltaTime;
        if (Displaytime >= 1)
        {
                GHide1.SetActive(false);
        }
        if (Displaytime >= 2) GHide2.SetActive(false);
        if (Displaytime >= 3) GHide3.SetActive(false);
        if (Displaytime >= 4) GHide4.SetActive(false);
        if (Displaytime >= 5)
        {
            GHide1.SetActive(true);
            GHide2.SetActive(true);
            GHide3.SetActive(true);
            GHide4.SetActive(true);
            Displaytime = 0;
          }
    }
    public string GetIPAddressText()
    {
        // IPAddress.txtを取得する
        FileInfo fi = new FileInfo(path);
        try
        {
            using(StreamReader sr = new StreamReader(fi.OpenRead(), Encoding.UTF8))
            {
                // テキストの中に書いている文字を取得する
                readTxt = sr.ReadToEnd();
            }
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }

        return readTxt;
    }
}
