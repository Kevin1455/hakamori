﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class NetworkChildMove : NetworkBehaviour
{
    AudioSource[] audioSource;
    [SerializeField]
    public AudioClip ChidSkillSE;
    public AudioClip running_child;

    private bool Titleback = false;//タイトルフラグ
    [SyncVar]
    private bool Canmoveflg = true;//操作可能フラグ
    private bool NPCspawnflg = false;//NPC子供生成フラグ
    private Vector3 NowPos = new Vector3(0, 0, 0);//現在の位置
    private Vector3 OldPos = new Vector3(0, 0, 0);//1フレーム前の位置
    private float Respawntime = 0;//リスポーンまでの時間
    private float NPCcooltime = 20;//NPC子供生成のクールタイム
    private CharacterController charaCon;//キャラクターコンポーネント用の変数
    private GameObject Player;//プレイヤーオブジェクト
    private GameObject Childbody;//子供の体
    private Animator animator;
    public bool Runflg;
    public bool Attackflg; // 墓を攻撃しているかどうか

    [SerializeField]
    GameObject ChildrenObject01;//NPC子供のプレハブ
    private GameObject ChildrenSpawn;//ChildrenSpawnオブジェクト
    public float Speed = 14.0f; //移動速度（Public＝インスペクタで調整可能）
    Quaternion Q;//子供の角度

    private readonly int[,] Childrenpos = new int[4, 4]//子供のリスポーン座標
    { //  x,  y,   z, 向き 
        {65,  0,  0,   -90},
        {0,  0,  65,   180},
        {-65,  0,  0,   90},
        {0,  0, -65, 0}
    };

    // Start is called before the first frame update
    void Start()
    {
        charaCon = GetComponent<CharacterController>(); // キャラクターコントローラーのコンポーネントを参照する
        ChildrenSpawn = GameObject.Find("ChildrenSpawn");//オブジェクトを取得
        audioSource = gameObject.GetComponents<AudioSource>();
        animator = GetComponent<Animator>();
        //自身の体を消す
        if (isLocalPlayer)
        {
            Childbody = GameObject.Find("PlyM_male1");
            Childbody.gameObject.SetActive(false);
            Player = null;
            GameObject.Find("UICamera/Canvas").GetComponent<SelectPlayer>().SetType(1);
            GameObject.Find("GraveHPCamera").gameObject.SetActive(false);//オブジェクトを取得
        }
    }

    [ClientCallback]
    void Update()
    {
        if(Player == null) Player = GameObject.Find("Player(Network)(Clone)");

        //クールタイムのタイマー
        if (NPCcooltime <= 15.0f) 
        { 
            NPCcooltime += Time.deltaTime; 
            NPCspawnflg = true;
        }
        //クールタイム終了のSE
        else if (NPCspawnflg)
        {
            audioSource[1].Play();
            NPCspawnflg = false;
        }
        //右クリックでNPC子供の生成
        else if (Input.GetButtonDown("Fire2") && isLocalPlayer && NPCcooltime >= 15.0f )
        {
            NPCSpawn();
            NPCcooltime = 0;
        }
        //カメラと位置同期
        CamMove();
        //墓守に捕まった場合
        if(!Canmoveflg) Dead();

        if(OldPos != NowPos)
        {
            Runflg = true;
            audioSource[1].UnPause();
            audioSource[0].UnPause();
        }
        else
        {
            Runflg = false;
            Attackflg = false;
            animator.SetBool("Idle", true);
            animator.SetBool("Run", false);
            audioSource[0].Pause();
            audioSource[1].Pause();
        }

        if (Runflg)
        {
            animator.SetBool("Run", true);
            animator.SetBool("Hammer", false);
            animator.SetBool("Idle", false);
            audioSource[0].Play();
        }

        if (Attackflg)
        {
            audioSource[0].Pause();
            audioSource[1].Pause();
            animator.SetBool("Hammer", true);
            animator.SetBool("Run", false);
            animator.SetBool("Idle", false);
        }
    }

    private void CamMove()
    {
        if (isLocalPlayer)
        {
            //カメラの位置を自身と同期させる
            Vector3 v;
            //捕まっていたら墓守、捕まっていなかったら子供の位置を格納
            if(Canmoveflg) v = gameObject.transform.position;
            else v = Player.transform.position;
            
            v.z += 0.1f;
            v.y += 2f;
            Camera.main.transform.position = v;

            Q = Camera.main.transform.rotation;
        }
    }

    [ClientCallback]
    private void FixedUpdate()
    {

        if (isLocalPlayer && Canmoveflg)
        {
            //移動処理
            var cameraForward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;//カメラが追従するための動作

            Vector3 direction = cameraForward * Input.GetAxis("Vertical") + Camera.main.transform.right * Input.GetAxis("Horizontal");//キーやスティックの入力

            if(Canmoveflg)CmdMoveSphere(direction, Q);//移動する動作の処理を実行する
        }
    }

    public bool IsLocalPlayer()
    {
        return isLocalPlayer;//クライアントであるフラグを返す
    }

    //勝敗決定後の操作
    public bool TitleBackLocal()
    {
        if(Input.GetButton("Fire1") && isLocalPlayer)
        {
            Titleback =true;
        }
        return Titleback;
    }
    
    //NPC子供の生成
    [Command]
    private void NPCSpawn()
    {
        GameObject NPC = Instantiate(ChildrenObject01, this.gameObject.transform.position, Quaternion.identity);
        NetworkServer.Spawn(NPC);
        NPC.transform.SetParent(ChildrenSpawn.transform, true);
    }

    //捕まった場合の処理
    private void Dead()
    {
        if(this.gameObject.transform.position != new Vector3(0,-256,0))
        {
            Corpse();
        }

        if(!Canmoveflg)Respawntime += Time.deltaTime;
        if(Respawntime >= 15)
        {
            Respawn(Random.Range(0, 3));
            Respawntime = 0;
        }
    }

    //子供の体を移動
    [Command]
    private void Corpse()
    {
        Player.GetComponent<NetworkPlayerMove>().ChildInvisible();
        this.gameObject.transform.position = new Vector3(0,-256,0);
        Debug.Log(gameObject.transform.position);
    }

    //リスポーン座標からランダムに選択された座標に移動
    [Command]
    private void Respawn(int Pos)
    {
        Player.GetComponent<NetworkPlayerMove>().ChildVisible();
        Canmoveflg = true;//操作可能に
        this.gameObject.transform.position = new Vector3(Childrenpos[Pos, 0],Childrenpos[Pos, 1],Childrenpos[Pos, 2]);
    }

    //墓守の手に触れられた場合
    [ClientCallback]
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            Canmoveflg = false;//操作不可能に
        }

        //接触したオブジェクトのタグが"gravestone"のとき
        if (other.gameObject.tag == "gravestone")
        {   
            Attackflg = true;
            Runflg = false;
        }
        //墓を壊したら
        else if (other.gameObject.tag == "gravestone_02")
        {
            Attackflg = false;
            audioSource[1].UnPause();
            audioSource[0].UnPause();
        }
    }

    //子供の移動
    [Command]
    public void CmdMoveSphere(Vector3 Moverange, Quaternion Q)
    {
        OldPos = this.gameObject.transform.position;//現在の位置を格納
        charaCon.Move(Moverange * Time.deltaTime * Speed);
        GetComponent<Transform>().transform.rotation = Q;//カメラと角度を同期
        this.gameObject.transform.position = new Vector3(gameObject.transform.position.x, 0.0f, gameObject.transform.position.z);
        NowPos = this.gameObject.transform.position;
    }
}
