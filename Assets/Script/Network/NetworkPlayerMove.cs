﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Mirror;

public class NetworkPlayerMove : NetworkBehaviour
{
    public static bool Plyscene;//プレイシーンかどうか

    AudioSource FireAudio;//ロウソクの火のオーディオソース
    [SerializeField]
    AudioClip FireSE;//ロウソク使用時のSE
    AudioSource CrossAudio;//十字架のオーディオソース
    public AudioClip CrossSE;//十字架使用時のSE
    private CharacterController charaCon;//キャラクターコンポーネント用の変数
    private bool Titleback = false;
    public float Speed = 5.0f; //移動速度（Public＝インスペクタで調整可能）
    public float kaitenSpeed = 1200.0f;//プレイヤーの回転速度（Public＝インスペクタで調整可能）
    private GameObject[] MatchNumImage;//マッチ残数UI
    private GameObject PlyChild;//クライアントの操作する子供の体
    private GameObject PlyPicker;//クライアントの操作する子供のつるはし
    GameObject MatchNumcln;//マッチ残数UIのクローン
    GameObject Candlefire;//ロウソクの火
    GameObject Matchandcandle;//マッチとロウソク
    GameObject Match;//マッチ
    GameObject Cross;//十字架
    GameObject Rockandtube;//石とチューブ
    GameObject Rock;//石オブジェクト
    GameObject Hand;//手
    GameObject Canvas;//キャンバス
    GameObject RepairCollider;//墓修復アイテム
    GameObject KillerCollider;//ゴースト消滅アイテム
    int matchnum;//マッチの残り本数
    private int Rocknum;//石の所持数
    int oldmatchnum;//1フレーム前のマッチ残数
    private int Havingitem;//手元に表示されているアイテム
    private int Haditem;//1フレーム前に持ってたアイテム
    float FireTime;//火の制限時間
    bool Fireflg;//火がついているか
    bool Handflg;//手が表示されているか
    private bool Usingitem;//アイテム使用中のフラグ
    private bool Usedkiller;//ゴースト消滅アイテム使用後trueに
    private bool Usedrepair;//墓修復アイテム使用後trueに

    private Vector3 movePower = Vector3.zero;    // キャラクター移動量（未使用）
    private const float gravityPower = 9.8f;         // キャラクター重力（未使用）


    // Start is called before the first frame update
    void Start()
    {
        //GameObject.Find("UICamera/Canvas").GetComponent<UIController>().SetPFlg(true);

        //タイトル画面でないならtrueに
        if (SceneManager.GetActiveScene().name == "TitleScene") Plyscene = false;
        else Plyscene = true;

        charaCon = GetComponent<CharacterController>(); // キャラクターコントローラーのコンポーネントを参照する 
        matchnum = 3;//マッチの初期値
        Rocknum = 3;

        //プレイ画面でのみ実行
        if (Plyscene && isLocalPlayer)
        {
            //オブジェクトを見つけて格納する
            Hand = GameObject.Find("Hand");
            Candlefire = GameObject.Find("Magic fire pro orange");
            FireAudio = Candlefire.GetComponent<AudioSource>();
            FireSE = (AudioClip)Resources.Load("Sound/matchfire");
            MatchNumImage = new GameObject[] { (GameObject)Resources.Load("Prefab/MatchNum1"),
                (GameObject)Resources.Load("Prefab/MatchNum2"), (GameObject)Resources.Load("Prefab/MatchNum3")};
            Canvas = GameObject.Find("MatchCanvas");
            PlyChild = null;
            PlyPicker = null;
            Matchandcandle = GameObject.Find("MatchandCandle");
            Match = GameObject.Find("MyMatchBox"); 
            Cross = GameObject.Find("Cross");
            CrossAudio = Cross.GetComponent<AudioSource>();
            CrossSE = (AudioClip)Resources.Load("Sound/Use Cross");
            Rockandtube = GameObject.Find("RockandTube");
            Rock = GameObject.Find("HaveRock_11");
            RepairCollider = GameObject.Find("Repair");
            KillerCollider = GameObject.Find("Cross/Killer");
            Havingitem = 0;
            Usedkiller = false;
            Usedrepair = false;
            Usingitem = false;

            Cross.SetActive(false);
            Rockandtube.SetActive(false);

            Candlefire.SetActive(false);//ロウソクの炎消しておく
            RepairCollider.SetActive(false);//修復アイテム非表示
            KillerCollider.SetActive(false);//ゴースト消滅アイテム非表示
        }

        // 自分が操作する場合に姿を消す
        if (isLocalPlayer)
        {
            GameObject.Find("13544_Black_Cape_v1_l3").gameObject.SetActive(false);
        }
        //クライアントの場合アイテムを非表示
        else
        {
            GameObject.Find("MatchandCandle").SetActive(false);
            GameObject.Find("Cross").SetActive(false);
            GameObject.Find("RockandTube").SetActive(false);
        }

        Fireflg = false;//初期値false
        Handflg = false;//初期値false
    }

    // Update is called once per frame
    void Update()
    {
        if (Plyscene && isLocalPlayer)PlayerAction();
        if(PlyChild == null || PlyPicker == null)
        {
            PlyChild = GameObject.Find("PlyM_male1");
            PlyPicker = GameObject.Find("Plybasic_rig");
        }

        //浮いてしまった場合の落下処理
        if (this.gameObject.transform.position.y > 3) this.gameObject.transform.position
                  = new Vector3(gameObject.transform.position.x, 3.0f, gameObject.transform.position.z);

        CameraMove();
    }

    private void PlayerAction()
    {
        //ロウソクの火がついている時間
        if (Fireflg) FireTime += Time.deltaTime;
        //RTが押されたら
        if (Input.GetButtonDown("Fire3") && isLocalPlayer)
        {
            Handflg = true;//Handflgをtrue
        }

        //目を閉じている、目を開けた瞬間の移動速度の処理
        if (Input.GetButton("Fire1") && isLocalPlayer)
        {
            Speed += 0.2f;
        }
        if (Input.GetButtonUp("Fire1") && isLocalPlayer)
        {
            Speed = 1.0f;
        }
        //通常の速度に戻す
        else
        {
            if (Speed < 7.0f) Speed += 0.1f;
        }

        //アイテムの切り替え
        if (isLocalPlayer)
        {
            Haditem = Havingitem;
            if(Usedkiller || Usedrepair)
            {
                Havingitem = 0;
                Usedkiller = false;
                Usedrepair = false;
            }

            if (Input.GetAxis("JoyUD") >= 0.5f && !Usingitem) Havingitem = 0;
            if (Input.GetAxis("JoyLR") >= 0.5f && !Usingitem && Rockandtube != null) Havingitem = 1;
            if (Input.GetAxis("JoyLR") <= -0.5f && !Usingitem && Cross != null) Havingitem = 2;
        }

        //アイテムの処理
        switch (Havingitem)
        {
            case 0:
                UseMatch();
                break;
            case 1:
                UseRepair();
                break;
            case 2:
                UseKiller();
                break;
            default:
                Debug.Log("Error Item");
                break;
        }

        if (Havingitem == 0) DisplayMatch();//手元のマッチ表示
        else if (Havingitem == 1) DisplayRepair();
        else if (Havingitem == 2) DisplayKiller();

        Debug.Log("Now Item Num = " + Havingitem);
        Debug.Log("Item using " + Usingitem);
    }
    
    [ClientCallback]
    private void CameraMove()
    {
        if (isLocalPlayer)
        {
            Vector3 v = gameObject.transform.position;
            v.y -= 0.4f;
            Camera.main.transform.position = v;

            Quaternion q = Camera.main.transform.rotation;
            this.gameObject.transform.rotation = q;
        }
    }

    [ClientCallback]
    private void FixedUpdate()
    {
        if (isLocalPlayer)
        {
            //移動処理
            var cameraForward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;//カメラが追従するための動作

            Vector3 direction = cameraForward * Input.GetAxis("Vertical") + Camera.main.transform.right * Input.GetAxis("Horizontal");//キーやスティックの入力

            Move(direction);//移動する動作の処理を実行する
        }
    }

    private void UseMatch()
    {
        //ロウソクに火をつける処理
        if (Input.GetButtonDown("Fire2") && !Handflg && matchnum > 0 && !Fireflg)
        {
            Candlefire.SetActive(true);
            //SE再生
            FireAudio.PlayOneShot(FireSE, 0.5f);
            Fireflg = true;
            Usingitem = true;
            matchnum--;
        }
        //10秒後に消える
        else if (FireTime >= 10)
        {
            Candlefire.SetActive(false);
            Fireflg = false;
            Usingitem = false;
            FireTime = 0;
        }
    }

    //墓修復アイテムの使用
    private void UseRepair()
    {
        if (Input.GetButton("Fire2") && !RepairCollider.activeSelf && Rocknum >= 1)
        {
            RepairCollider.SetActive(true);
            Usingitem = true;
        }
        else if (Input.GetButtonUp("Fire2"))
        {
            RepairCollider.SetActive(false);
            Usingitem = false;
        }
    }

    //ゴースト消滅アイテムの使用
    private void UseKiller()
    {
        if (Input.GetButtonUp("Fire2") && Usingitem && KillerCollider.activeSelf)
        {
            KillerCollider.SetActive(false);
            Usingitem = false;
        }
        else if (Input.GetButtonDown("Fire2") && !KillerCollider.activeSelf)
        {
            KillerCollider.SetActive(true);
            Usingitem = true;
        }

        if (Input.GetButton("Fire2"))
        {
            if(!CrossAudio.isPlaying) CrossAudio.PlayOneShot(CrossSE, 0.5f);
        }
    }

    public void SetUsedKiller()
    {
        Usedkiller = true; 
        Usingitem = false;
        Destroy(Cross);
        Cross = null;
    }

    //墓修復アイテム(石)の所持数を減らす
    //GraveControllerから呼び出す
    public void RepairedGrave()
    {
        RepairCollider.SetActive(false);
        Rocknum--;
        if(Rocknum <= 0)
        {
            Usedrepair = true;
            Destroy(Rockandtube);
            Rockandtube = null;
        }
    }

    private void DisplayMatch()
    {
        if (Havingitem != Haditem)
        {
            Matchandcandle.SetActive(true);
            if (Haditem == 1 && Rockandtube != null) Rockandtube.SetActive(false);
            if (Haditem == 2 && Cross != null) Cross.SetActive(false);
        }

        //マッチの残りの本数で見た目を変える
        //0なら箱ごと消す
        if (matchnum <= 0 || Handflg)
        {
            Match.SetActive(true);
            Match = GameObject.Find("MyMatchBox");
            Match.SetActive(false);
        }
        //全部表示する
        else
        {
            Match.SetActive(true);
        }

        //マッチ残数UIの表示
        if (oldmatchnum != matchnum)
        {
            if (matchnum != 0)
            {
                Destroy(MatchNumcln, 0.1f);
                MatchNumcln = Instantiate(MatchNumImage[matchnum - 1], new Vector3(0, 0, 0), Quaternion.identity);
                MatchNumcln.transform.SetParent(Canvas.transform, false);
            }
            else
            {
                Destroy(MatchNumcln, 0.1f);
            }
            oldmatchnum = matchnum;
        }
    }

    //墓修復アイテムの表示
    private void DisplayRepair()
    {
        if (Havingitem != Haditem)
        {
            Rockandtube.SetActive(true);
            if (Haditem == 0) Matchandcandle.SetActive(false);
            if (Haditem == 2) Cross.SetActive(false);
        }
        if (Rocknum <= 0 || Handflg) Rock.SetActive(false);
        else Rock.SetActive(true);
    }

    //ゴースト消滅アイテムの表示
    private void DisplayKiller()
    {
        if (Havingitem != Haditem)
        {
            Cross.SetActive(true);
            if (Haditem == 0) Matchandcandle.SetActive(false);
            if (Haditem == 1) Rockandtube.SetActive(false);
        }
        if (Handflg) Cross.SetActive(false);
        else Cross.SetActive(true);
    }

    public bool TitleBackLocal()
    {
        if (Input.GetButton("Fire1") && isLocalPlayer)
        {
            Titleback = true;
        }
        return Titleback;
    }


    // ■移動する動作の処理
    [Command]
    public void Move(Vector3 Moverange)
    {
        charaCon.Move(Moverange * Time.deltaTime * Speed);   // プレイヤーの移動距離は時間×移動スピードの値
    }

    //マッチを拾ったら残数を1増やす
    public void MatchCounter()
    {
        if (matchnum < 3) matchnum++;
    }
    //手フラグのSet関数
    public void SetHandFlg(bool Flg)
    {
        Handflg = Flg;
    }

    //ホスト側のみ捕まえた子供の体を非表示
    public void ChildInvisible()
    {
        if (isLocalPlayer)
        {
            PlyChild.SetActive(false);
            PlyPicker.SetActive(false);
        }
    }
    
    //ホスト側のみリスポーンした子供の体を表示
    public void ChildVisible()
    {
        if (isLocalPlayer)
        {
            PlyChild.SetActive(true);
            PlyPicker.SetActive(true);
        }
    }
}
