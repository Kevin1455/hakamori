﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Mirror;

[AddComponentMenu("")]
public class MyNetworkRoomPlayer : NetworkRoomPlayer
{
    static readonly ILogger logger = LogFactory.GetLogger(typeof(MyNetworkRoomPlayer));

    public override void OnStartClient()
    {
        if (logger.LogEnabled()) logger.LogFormat(LogType.Log, "OnStartClient {0}", SceneManager.GetActiveScene().path);

        base.OnStartClient();
    }

    public override void OnClientEnterRoom()
    {
        if (logger.LogEnabled()) logger.LogFormat(LogType.Log, "OnClientEnterRoom {0}", SceneManager.GetActiveScene().path);
    }

    public override void OnClientExitRoom()
    {
        if (logger.LogEnabled()) logger.LogFormat(LogType.Log, "OnClientExitRoom {0}", SceneManager.GetActiveScene().path);
    }

    public override void ReadyStateChanged(bool _, bool newReadyState)
    {
        if (logger.LogEnabled()) logger.LogFormat(LogType.Log, "ReadyStateChanged {0}", newReadyState);
    }

    public override void OnGUI()
    {
        if (!showRoomGUI)
            return;

        NetworkRoomManager room = NetworkManager.singleton as NetworkRoomManager;
        if (room)
        {
            // RoomSceneのみ下記の処理を実行する
            if (!NetworkManager.IsSceneActive(room.RoomScene))
                return;

            // 各プレイヤーの開始準備状態を示すGUI
            DrawPlayerReadyState();

            // プレイヤーが開始準備を切り替えるGUI
            DrawPlayerReadyButton();
        }
    }

    void DrawPlayerReadyState()
    {
        GUILayout.BeginArea(new Rect(70f + (index * 100), 200f, 90f, 130f));

        // ネットIDを取得
        uint Netid = gameObject.GetComponent<NetworkIdentity>().netId;
        
        // 自分の名前の色を赤くする
        if(isLocalPlayer)GUI.contentColor = Color.red;

        // ネットIDが1なら"HAKAMORI"、それ以外は"CHILD"の文字を表示する
        if (Netid == 1) GUILayout.Label("  HAKAMORI");
        else GUILayout.Label("      CHILD");

        // 文字を白に戻す
        if (isLocalPlayer) GUI.contentColor = Color.white;

        // 各プレイヤーの準備状態を表示する
        if (readyToBegin)
            GUILayout.Label("      Ready  ");
        else
            GUILayout.Label("   Not Ready");

        // ホストのみ、入ってきたプレイヤーの接続を切れるようにする
        if (((isServer && index > 0) || isServerOnly) && GUILayout.Button("REMOVE"))
        {
            // This button only shows on the Host for all players other than the Host
            // Host and Players can't remove themselves (stop the client instead)
            // Host can kick a Player this way.
            GetComponent<NetworkIdentity>().connectionToClient.Disconnect();
        }

        GUILayout.EndArea();
    }

    void DrawPlayerReadyButton()
    {
        if (NetworkClient.active && isLocalPlayer)
        {
            GUILayout.BeginArea(new Rect(210f, 420f, 180f, 180f));

            if (readyToBegin)
            {
                // 準備中の状態にする
                if (GUILayout.Button("Cancel"))
                    CmdChangeReadyState(false);
            }
            else
            {
                // 準備完了の状態にする
                if (GUILayout.Button("Ready"))
                    CmdChangeReadyState(true);
            }

            GUILayout.EndArea();
        }
    }
}
