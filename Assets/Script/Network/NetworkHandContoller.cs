﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkHandContoller : MonoBehaviour
{
    Animator animator;//アニメーター
    GameObject Player;//プレイヤー
    GameObject Handcol;//手の当たり判定
    NetworkPlayerMove Plycon;//プレイヤーコントローラスクリプト
    float Handcooltime;//クールタイム

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.Find("Player(Network)(Clone)");//プレイヤーオブジェクトを格納
        Handcol = GameObject.Find("HandCollider");//手の当たり判定オブジェクトを格納
        animator = this.gameObject.GetComponent<Animator>();//アニメーターを取得
        Plycon = Player.GetComponent<NetworkPlayerMove>();//プレイヤーコントローラを取得
        Handcol.SetActive(false);//手の当たり判定を無効
        Handcooltime = 0;//初期値0
    }

    // Update is called once per frame
    void Update()
    {
        //RTが押されたら
        if (Input.GetButtonDown("Fire3"))
        {
            animator.SetBool("Handanm", true);//アニメーション再生
            Handcol.SetActive(true);//手の当たり判定を有効
        }
        if (Handcooltime < 2.0f && animator.GetBool("Handanm")) Handcooltime += Time.deltaTime;//クールタイム
        else if (Handcooltime >= 2.0f && animator.GetBool("Handanm"))
        {
            animator.SetBool("Handanm", false);//アニメーション再生
            Handcooltime = 0;//クールタイムのリセット
            Plycon.SetHandFlg(false);//プレイヤーコントローラのHandflgをfalseに
        }
    }

    //アニメーション再生終了後に呼び出される
    public void HandActiveFalse()
    {
        Handcol.SetActive(false);//手の当たり判定を無効
    }
}
