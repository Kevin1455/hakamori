﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkCameraContoller : MonoBehaviour
{
    public NetworkCameraEffect Cameraef;//CameraEffectのコンポーネント
    public Transform target;//ターゲット（プレイヤー）
    public float distance = 12.0f;//距離
    public float xSpeed = 25.0f;//横の回転速度
    public float ySpeed = 12.0f;//縦の回転速度
    public float yMinLimit = -45f;//横の最大角度
    public float yMaxLimit = 85f;//縦の最大角度
    
    //private bool isCliant;
    private float x = 0.0f;//横の回転角度
    private float y = 0.0f;//縦の回転角度
    private float Limittime;//CameraEffectをtrueにするまでの時間
    private float Nowtime;//現在の経過時間

    void Start()
    {
        Cameraef = GetComponent<NetworkCameraEffect>();//コンポーネントを取得
        var angles = transform.eulerAngles;//角度を取得
        x = angles.y;//現在の角度を取得
        y = angles.x;//現在の角度を取得
        Limittime = 10;//時間を設定
        Nowtime = 0;//初期値0を代入
        target = null;
        //isCliant = false;
        //プレイ画面のみ開始時にtrue
        if (SceneManager.GetActiveScene().name != "TitleScene") Cameraef.enabled = true;
    }

    void Update()
    {
        //Debug.Log(Cameraef.GetisPlayer() + ":CamCon");
        
        var controllerNames = Input.GetJoystickNames();//コントローラが接続されていたら、コントローラ名を入れる

        string controllerName = "";
        int i = 0;
        //コントローラ名の文字数を格納
        while (controllerNames.Length > i && controllerNames[i] == "")
        {
            i++;
        }
        //stringに格納
        if (controllerNames.Length > i)
        {
            controllerName = controllerNames[i];
        }

        if (target)
        {
            //コントローラの右スティックの名前を格納

            var XAxis = "R_Stick_H";
            var YAxis = "R_Stick_V";
            if (controllerName.IndexOf("Generic") != -1)
            {
                XAxis = "R_Stick_H2";
            }
            //回転処理
            if (controllerName.IndexOf("Generic") != -1)
            {
                x += Input.GetAxis(XAxis) * xSpeed * 0.12f;
                y += Input.GetAxis(YAxis) * ySpeed * 0.08f;
            }
            else
            {
                x += Input.GetAxis(XAxis) * xSpeed * 0.02f;
                y += Input.GetAxis(YAxis) * ySpeed * 0.02f;
            }
            y = ClampAngle(y, yMinLimit, yMaxLimit);

            var rotation = Quaternion.Euler(y, x, 0);
            var position = rotation * new Vector3(0.0f, 0.0f, -distance) + target.position;

            transform.rotation = rotation;
            transform.position = position;
            
        }

        //タイトルシーンでないなら処理を行う
        if (SceneManager.GetActiveScene().name != "TitleScene")
        {
            //10秒後にCameraEffectのenebledをtrueに
            if (!Cameraef.enabled) Nowtime += Time.deltaTime;
            if (Nowtime > Limittime && !Cameraef.enabled)
            {
                Cameraef.enabled = true;
            }
            //瞬きのボタンが押されたら
            if (Input.GetButton("Fire1"))
            {
                Cameraef.enabled = true;
                Cameraef.Closeeye();
            }
            //瞬きのボタンが離されたら
            if (Input.GetButtonUp("Fire1") && Cameraef.enabled)
            {
                Cameraef.Set_iteration();
                Cameraef.enabled = false;
                Nowtime = 0;
            }
        }
    }

    private void FixedUpdate()
    {
        target = this.gameObject.transform;
    }

    //カメラの回転を行う
    static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360) { angle += 360; }
        if (angle > 360) { angle -= 360; }
        return Mathf.Clamp(angle, min, max);
    }
}
