﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Runtime.InteropServices;

public class MouseMover : MonoBehaviour
{
    //[DllImport("user32.dll")]
    //public static extern bool SetCursorPos(int x, int y);

    //[DllImport("user32.dll", CallingConvention = CallingConvention.StdCall)]
    //public static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);

    private const int MOUSEEVENTF_LEFTDOWN = 0x2;
    private const int MOUSEEVENTF_LEFTUP = 0x4;
    private float angle;
    private int x;
    private int y;
    private int WINDOW_MAX_SIZE_X = 1920;
    private int WINDOW_MAX_SIZE_Y = 1080;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        x = 960;
        y = 500;
    }

    // Update is called once per frame
    void Update()
    {
        var controllerNames = Input.GetJoystickNames();//コントローラが接続されていたら、コントローラ名を入れる

        string controllerName = "";
        int i = 0;
        //コントローラ名の文字数を格納
        while (controllerNames.Length > i && controllerNames[i] == "")
        {
            i++;
        }
        //stringに格納
        if (controllerNames.Length > i)
        {
            controllerName = controllerNames[i];
        }

        if (controllerName.IndexOf("Joystick") != -1)
        {
            x += (int)Input.GetAxis("Horizontal") * 10;
            y -= (int)Input.GetAxis("Vertical") * 10;
        }
        else
        {
            x += (int)Input.GetAxis("Horizontal") * 10;
            y -= (int)Input.GetAxis("Vertical") * 10;
        }


        if(SceneManager.GetActiveScene().name == "LobbyScene")
        {
            if (controllerName.IndexOf("Generic") != -1) angle += Input.GetAxis("R_Stick_H2");
            else angle += Input.GetAxis("R_Stick_H");
            this.gameObject.transform.rotation = Quaternion.Euler(0, angle, 0);
        }

        //上限、下限設定
        if(x >= WINDOW_MAX_SIZE_X) x = WINDOW_MAX_SIZE_X;
        if(y >= WINDOW_MAX_SIZE_Y) y = WINDOW_MAX_SIZE_Y;
        if(x <= 0) x = 0;
        if(y <= 0) y = 0;

        //if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) SetCursorPos(x, y);

        //if(Input.GetButtonDown("Fire1"))mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
        //if(Input.GetButtonUp("Fire1")) mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
    }
}
