﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchController : MonoBehaviour
{
    [SerializeField]
    GameObject Player;
    GameObject Objectspawn; // ObjectSpawnのスクリプトを呼び出す用

    AudioSource audioSource;
    public AudioClip Pickupmatch;

    // Start is called before the first frame update
    void Start()
    {
        // GameObjectのObjectSpawnを見つける
        Objectspawn = GameObject.Find("ObjectSpawn");

        if(!Objectspawn.GetComponent<ObjectSpawn>().GetOnlineFlg())Player = GameObject.Find("Player");
        else Player = GameObject.Find("Player(Network)(Clone)");

        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    /*
    // マッチ箱を削除する関数
    void OnTriggerEnter(Collider other)
    {
        //接触したオブジェクトのタグが"Player"のとき
        if (other.gameObject.tag == "Player")
        {
            audioSource.PlayOneShot(Pickupmatch, 1.0f);

            // プレイヤーが所持しているマッチの数を増やす
            if (!Objectspawn.GetComponent<ObjectSpawn>().GetOnlineFlg()) Player.GetComponent<PlayerController>().MatchCounter();
            else Player.GetComponent<NetworkPlayerMove>().MatchCounter();

            // ObjectSpawnの関数を呼ぶ
            Objectspawn.GetComponent<ObjectSpawn>().SpawnPlace(gameObject.transform);

            // マッチのオブジェクトを消す
            Destroy(gameObject, 0.2f);
            
        }
    }
    */
}
