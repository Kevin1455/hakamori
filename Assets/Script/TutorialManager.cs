﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialManager : MonoBehaviour
{
    public CameraEffect CameraTE;//CameraEffectのコンポーネント
    public CameraController CameraTC;//CameraEffectのコンポーネント
    Animator animator;
    Animator animator2;
    Animator animator3;
    Animator animator4;
    Animator animator5;
    Animator animator6;
    GameObject TText1;//タイトルロゴ
    GameObject TText2;//操作説明
    GameObject TText3;//操作説明
    GameObject TText4;//操作説明
    GameObject TText5;//操作説明
    GameObject TText6;//操作説明
    GameObject TBack;
    GameObject TNext;
    GameObject Objectspawn;
    bool Cntdisplay;//操作説明が表示されているか
    float Displaytime;//表示時間
    float Tutorialtime;//表示時間
    [SerializeField]
    private GameObject Player;
    private int TextCount = 0; 
    private GameObject UIC;
    private bool Playflg = false;
    private GameObject TChildren;
    private GameObject TGhosts;
    // Start is called before the first frame update
    void Start()
    {
        CameraTE = Camera.main.GetComponent<CameraEffect>();//コンポーネントを取得
        CameraTC = Camera.main.GetComponent<CameraController>();//コンポーネントを取得
        Player = GameObject.Find("Player");
        Player.SetActive(false);
        UIC = GameObject.Find("UICamera");
        UIC.SetActive(false);
        TBack = GameObject.Find("Titleback");
        TBack.SetActive(false);
        TNext = GameObject.Find("Next");
        TNext.SetActive(false);
        Objectspawn = GameObject.Find("ObjectSpawn");
        Objectspawn.SetActive(false);
        TText1 = GameObject.Find("TText1");//タイトルロゴオブジェクトを格納
        TText2 = GameObject.Find("TText2");//操作説明オブジェクトを格納
        TText3 = GameObject.Find("TText3");//タイトルロゴオブジェクトを格納
        TText4 = GameObject.Find("TText4");//操作説明オブジェクトを格納
        TText5 = GameObject.Find("TText5");//タイトルロゴオブジェクトを格納
        TText6 = GameObject.Find("TText6");//タイトルロゴオブジェクトを格納
        animator = TText1.GetComponent<Animator>();//タイトルロゴのアニメーターを取得
        animator2 = TText2.GetComponent<Animator>();//操作説明のアニメーターを取得
        animator3 = TText3.GetComponent<Animator>();//タイトルロゴのアニメーターを取得
        animator4 = TText4.GetComponent<Animator>();//操作説明のアニメーターを取得
        animator5 = TText5.GetComponent<Animator>();//タイトルロゴのアニメーターを取得
        animator6 = TText6.GetComponent<Animator>();//タイトルロゴのアニメーターを取得
        TChildren = GameObject.Find("TChildren");//
        TChildren.SetActive(false);
        TGhosts = GameObject.Find("TGhosts");//
        TGhosts.SetActive(false);
        Displaytime = 0;//初期値0
    }

    // Update is called once per frame
    void Update()
    {
        Displaytime += Time.deltaTime;

        //5秒後に操作説明を消してタイトルロゴを表示
        if (Displaytime >= 5 && TextCount < 6)
        {
            TNext.SetActive(true);
            if (Input.GetButton("Fire1"))
            {
                TNext.SetActive(false);
                animator.SetBool("TitleAnm", true);//タイトルロゴのアニメーション
                switch (TextCount)
                {
                    case 0:
                        animator2.SetBool("TitleCnt Bool", true);//操作説明のアニメーション
                        TextCount++;
                        break;

                    case 1:
                        animator2.SetBool("TitleCnt Bool", false);//操作説明のアニメーション
                        animator3.SetBool("TitleCnt Bool", true);//操作説明のアニメーション
                        TextCount++;
                        break;

                    case 2:
                        animator3.SetBool("TitleCnt Bool", false);//操作説明のアニメーション
                        animator4.SetBool("TitleCnt Bool", true);//操作説明のアニメーション
                        TextCount++;
                        break;

                    case 3:
                        animator4.SetBool("TitleCnt Bool", false);//操作説明のアニメーション
                        animator5.SetBool("TitleCnt Bool", true);//操作説明のアニメーション
                        TextCount++;
                        break;
                    case 4:
                        animator5.SetBool("TitleCnt Bool", false);//操作説明のアニメーション
                        animator6.SetBool("TitleCnt Bool", true);//操作説明のアニメーション
                        TextCount++;
                        break;
                    case 5:
                        TextCount++;
                        Objectspawn.SetActive(true);
                        break;
                }
                Displaytime = 0;//時間をリセット
            }
        }
        if(TextCount == 6)
        {
            CameraTE.enabled = true;
            CameraTE.Closeeye();
            CameraTC.enabled = false;
            Player.gameObject.transform.position = new Vector3(0, 3, 0);
            if (Displaytime >= 2)
            {
                CameraTC.enabled = true;
                Player.SetActive(true);
                UIC.SetActive(true);
                TextCount++;
                TChildren.SetActive(true);
                TGhosts.SetActive(true);
                Playflg = true;
                TBack.SetActive(true);
                //CameraTE.Set_iteration();
                CameraTE.enabled = false;
            }
        }
        if(Playflg == true)
        {
            Tutorialtime += Time.deltaTime;
            if (Input.GetButton("Menu"))
            {
                SceneManager.LoadScene("TitleScene");
            }
        }
    }
}
