﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RangeObject : MonoBehaviour
{
    // コサイン値を入れる器
    private float Searchcostheta;

    // 対戦モードフラグ
    private bool Onlineflg;

    // プレイヤー
    private GameObject Player;

    // 逃げる時に子供が追っているオブジェクト
    private GameObject Searchobject;

    // 親オブジェクトが持っているChildrenControllerを取得する用
    private ChildrenController Childrencontroller;

    // SphereColliderを取得する用
    private SphereCollider Gizmospherecollider;

    // 逃げる時にSphereColliderに触れたEscapeObjectを取得する用
    private List<GameObject> Findescapelist = new List<GameObject>();
    

    private void Start()
    {
        // このオブジェクトについているSphereColliderの情報を取得する
        Gizmospherecollider = GetComponent<SphereCollider>();

        // 90度を度からラジアンに変更する
        float Searchrad = 90 * Mathf.Deg2Rad;
        Searchcostheta = Mathf.Cos(Searchrad);

        // シーンがオンラインシーンならフラグをtrueにする
        if (SceneManager.GetActiveScene().name == "OnlineScene")
        {
            Onlineflg = true;
        }
        // それ以外ならフラグをfalseにする
        else Onlineflg = false;

        // オンラインシーンじゃない場合、Playerのオブジェクトを取得する
        if(!Onlineflg)Player = GameObject.Find("Player");
        else Player = GameObject.Find("Player(Network)(Clone)");

        // 親オブジェクトのChildrenControllerを取得する
        Childrencontroller = GetComponentInParent<ChildrenController>();
    }


    private void Update()
    {
        if(Onlineflg && Player == null) Player = GameObject.Find("Player(Network)(Clone)");
        // ギズモの向きを親オブジェクトの向きと合わせる
        gameObject.transform.rotation = transform.parent.rotation;

        // Findescapelistに入っているEscapeObjectの整理
        UpdateFoundEscape();
    }

    private void OnTriggerEnter(Collider other)
    {
        // 子供がプレイヤーを見つけたら(コライダーが触れたら)
        if(other.gameObject.tag == "MainCamera")
        {
            // コライダーの半径を広げる
            Gizmospherecollider.radius = 13.0f;

            // プレイヤーがいる位置と反対方向を向く
            PlayerFind();

            // 子供の逃げるフラグをtrueにする
            Childrencontroller.SetEscapeFlg(true);
        }
    }

    private void PlayerFind()
    {
        // 子供を中心として、プレイヤーの当たった角度を調べる
        Vector3 Playerpos = Player.transform.position;
        float tanz = Playerpos.z - transform.parent.position.z;
        float tanx = Playerpos.x - transform.parent.position.x;
        float Angle = Mathf.Atan2(tanz, tanx) * Mathf.Rad2Deg;

        // inspectorのrotationに出した角度を合わせる
        if (Angle < -90) Angle += 270;
        else Angle -= 90;
        Angle *= -1;

        // プレイヤーがいる位置と反対方向を向く
        transform.parent.rotation = Quaternion.Euler(0, Angle + 180, 0);
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "MainCamera")
        {
            // コライダーの半径を広げる
            Gizmospherecollider.radius = 13.0f;

            // 子供の逃げるフラグをtrueにする
            Childrencontroller.SetEscapeFlg(true);
        }

        if(other.gameObject.tag == "Player")
        {
            // プレイヤーが子供の正面に移動しようとしたとき
            if (ChildEscape(other.gameObject))
            {
                // プレイヤーがいる位置と反対方向を向く
                PlayerFind();

                // EscapeObjectを追っている最中なら新しく向いている方向のEscapeObjectを追う
                Childrencontroller.SetArrivalFlg(true);
            }
        }

        // コライダーにEscapeObjectが触れたとき、listに入れる
        if (other.gameObject.tag == "Escape")
        {
            // 触れたオブジェクトの情報を取得する
            GameObject Escapeobject = other.gameObject;

            // 多重登録されないようにする
            if (Findescapelist.Find(value => value == Escapeobject) == null)
            {
                // 触れたオブジェクトをListに追加する
                Findescapelist.Add(Escapeobject);
            }
        }
    }

    private void UpdateFoundEscape()
    {
        // Listをコピーする
        List<GameObject> FindTemp = Findescapelist;

        // listに入っているものすべてを確認する
        foreach (var Finddata in FindTemp.ToArray())
        {
            // List内のEscapeObjectを1つ取得する
            GameObject Targetobjet = Finddata;

            // 中身がnullならcontinueする
            if (Targetobjet == null) continue;

            // EscapeObjectが決められた範囲に入っているかを判断する
            bool Find = ChildEscape(Targetobjet);
            
            // ターゲットが範囲外もしくは現在追っているオブジェクトの場合、Listから外す
            if (!Find || (Targetobjet == Searchobject)) Findescapelist.Remove(Targetobjet);
        }
    }

    private bool ChildEscape(GameObject TargetObject)
    {
        // ターゲットのポジションを得る
        Vector3 Targetposition = TargetObject.transform.position;

        // 子供のポジションを得る
        Vector3 Myposition = transform.parent.position;

        // 子供のxz座標の大きさを得る
        Vector3 Mypositionxz = Vector3.Scale(Myposition, new Vector3(1.0f, 0.0f, 1.0f));

        // ターゲットのxz座標の大きさを得る
        Vector3 Targetpositionxz = Vector3.Scale(Targetposition, new Vector3(1.0f, 0.0f, 1.0f));

        // 子供からターゲットへの正規化したベクトルを得る
        Vector3 Targetdir = (Targetpositionxz - Mypositionxz).normalized;

        // 子供の正面を得る
        Vector3 Myforward = transform.parent.forward;

        // ギズモの範囲に入っていない場合、falseを返す
        if (!RangeAngle(Myforward, Targetdir, Searchcostheta)) return false;
        
        // それ以外の場合、trueを返す
        return true;
    }
    
    private bool RangeAngle(Vector3 MyForward, Vector3 TargetDir, float SearchCosTheta)
    {
        // 正規化したベクトルがない場合、同位置にあるものだとする
        if(TargetDir.sqrMagnitude <= Mathf.Epsilon) return true;

        // 子供の正面のベクトルと子供からターゲットへの正規化したベクトルの内積
        float dot = Vector3.Dot(MyForward, TargetDir);

        // 内積がコサイン値を上回っていればtrueを返す
        return dot >= SearchCosTheta;
    }

    public void SetOnlineFlg(bool Flg)
    {
        Onlineflg = Flg;
    }

    private void OnTriggerExit(Collider other)
    {
        // 子供がプレイヤーから離れたら(コライダーが離れたら)
        if (other.gameObject.tag == "MainCamera")
        {
            // コライダーの半径を縮める
            Gizmospherecollider.radius = 5.0f;
            Childrencontroller.SetEscapeFlg(false);
        }

        // コライダーからEscapeObjectが離れたら
        if (other.gameObject.tag == "Escape")
        {
            // 触れたオブジェクトの情報を取得する
            GameObject Exitobject = other.gameObject;

            // 該当するオブジェクトがlistに入っている場合、データを入れる
            var Finddata = Findescapelist.Find(value => value == Exitobject);

            // 中身がnullなら処理を飛ばす
            if(Finddata == null) return;

            // Listから外す
            Findescapelist.Remove(Finddata);
        }
    }
    
    public Transform GetEscapeTarget()
    {
        // Listの中身をコピーする
        List<GameObject> Copylist = new List<GameObject>(Findescapelist);

        // もし、Listの中身がなかった場合
        if (Copylist.Count <= 0)
        {
            // 逃げてるフラグをfalseにする
            Childrencontroller.SetEscapeFlg(false);

            // エラー防止のため自分の座標を送る
            return gameObject.transform.parent;
        }

        // Listの数分からランダムに数字を取得する
        int Rand = Random.Range(0, Copylist.Count);

        // 追うオブジェクトを決める
        Searchobject = Copylist[Rand];

        // Listから追うオブジェクトを外す
        Findescapelist.Remove(Searchobject);

        // listのオブジェクトのポジションを返す
        return Copylist[Rand].transform;
    }
}
