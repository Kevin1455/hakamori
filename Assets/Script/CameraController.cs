﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraController : MonoBehaviour
{
    public CameraEffect Cameraef;//CameraEffectのコンポーネント
    public Transform target;//ターゲット（プレイヤー）
    public float distance = 12.0f;//距離
    public float xSpeed = 250.0f;//横の回転速度
    public float ySpeed = 120.0f;//縦の回転速度
    public float yMinLimit = -60f;//横の最大角度
    public float yMaxLimit = 50f;//縦の最大角度
    public static float sensix1;
    public static float sensiy1;
    public static float sensix2;
    public static float sensiy2;
    private float x = 0.0f;//横の回転角度
    private float y = 0.0f;//縦の回転角度
    private float Limittime;//CameraEffectをtrueにするまでの時間
    private float Nowtime;//現在の経過時間
    private bool Moveflg;//メニューウィンドウを開いている場合false
    private int SensiNum = 1;

    private bool Eyeclose;
    private float Closeinterbal;
    private GameObject Skyboxchange;
    private GameObject Closeeye;

    private GameObject Player;
    private GameObject Flashlight;
    private GameObject UICamera;
    private GameObject EnemySpawn;
    private Animator animator;
    private AnimatorStateInfo stateInfo;
    private bool waitstart;
    private int HandWait;
    private GameObject Hand;
    void Start()
    {
        Hand = GameObject.Find("Hand");
        Cameraef = GetComponent<CameraEffect>();//コンポーネントを取得
        var angles = transform.eulerAngles;//角度を取得
        x = angles.y;//現在の角度を取得
        y = angles.x;//現在の角度を取得
        Limittime = 10f;//時間を設定
        Nowtime = 0;//初期値0を代入
        Moveflg = true;

        HandWait = 0;

        Eyeclose = false;
        Closeinterbal = 0f;

        Closeeye = GameObject.Find("EyeClose");
        Hand = GameObject.Find("Hand");

        //プレイ画面のみ開始時にtrue
        if (SceneManager.GetActiveScene().name == "PlayScene") 
        {
            Cameraef.enabled = false;
            Skyboxchange = GameObject.Find("SkyboxChange");

            Moveflg = false;
            waitstart = true;
            Player = GameObject.Find("Player");
            Player.SetActive(false);
            Flashlight = GameObject.Find("Flashlight");
            Flashlight.SetActive(false);
            UICamera = GameObject.Find("UICamera");
            UICamera.SetActive(false);
            EnemySpawn = GameObject.Find("EnemySpawn");
            EnemySpawn.SetActive(false);
            animator = gameObject.GetComponent<Animator>();

            animator.SetBool("wait", true);
        }
        if (SceneManager.GetActiveScene().name != "TitleScene") Cameraef.SetTitleScene(false);
    }

    void Update()
    {
        if(waitstart) stateInfo = animator.GetCurrentAnimatorStateInfo(0);

        if (stateInfo.normalizedTime >= 1.0f && waitstart)
        {
            Moveflg = true;
            waitstart = false;
            Flashlight.SetActive(true);
            Player.SetActive(true);
            UICamera.SetActive(true);
            EnemySpawn.SetActive(true);

            animator.SetBool("wait", false);
        }


        if(Moveflg && !waitstart)
        {
            //瞬きのボタンが押されたら
            if (Input.GetButtonDown("Fire1") && Closeinterbal == 0)
            {
                Eyeclose = true;
                Closeeye.GetComponent<EyeClose>().CloseAnimation(true);

                Hand.GetComponent<HandController>().HandAnimation(false);

                if (SceneManager.GetActiveScene().name == "PlayScene")
                {
                    Skyboxchange.GetComponent<SkyboxChange>().ChangeMaterial();
                }
            }

            if (Eyeclose)
            {
                Closeinterbal += Time.deltaTime;
                Cameraef.enabled = true;
                Cameraef.Closeeye();
                if (SceneManager.GetActiveScene().name == "TitleScene")
                {
                    Hand.SetActive(false);
                }
            }

            //瞬きのボタンが離されたら
            if (Closeinterbal >= 0.5f)
            {
                Closeeye.GetComponent<EyeClose>().CloseAnimation(false);
                Eyeclose = false;
                Closeinterbal = 0;
                Cameraef.Set_iteration();
                Cameraef.enabled = false;
                Nowtime = 0;
                if (SceneManager.GetActiveScene().name == "TitleScene")
                {
                    Hand.SetActive(true);
                }
            }
        }
        else if (!Moveflg && !waitstart)
        {
            Closeeye.GetComponent<EyeClose>().CloseAnimation(false);
            Eyeclose = false;
            Closeinterbal = 0;
            Cameraef.Set_iteration();
            Cameraef.enabled = false;
            Nowtime = 0;
        }


        if (Input.GetKey(KeyCode.Return))
        {
            SensiNum = 0;
            Debug.Log(SensiNum);
        }
        if (SensiNum == 1)
        {
            sensix1 = 1.2f;
            sensiy1 = 0.8f;
            sensix2 = 0.2f;
            sensiy2 = 0.2f;
        }
        else
        {
            sensix1 = 0.12f;
            sensiy1 = 0.08f;
            sensix2 = 0.02f;
            sensiy2 = 0.02f;
        }


        var controllerNames = Input.GetJoystickNames();//コントローラが接続されていたら、コントローラ名を入れる

        string controllerName = "";
        int i = 0;
        //コントローラ名の文字数を格納
        while (controllerNames.Length > i && controllerNames[i] == "")
        {
            i++;
        }
        //stringに格納
        if (controllerNames.Length > i)
        {
            controllerName = controllerNames[i];
        }

        if (target && Moveflg)
        {
            //コントローラの右スティックの名前を格納

            var XAxis = "R_Stick_H";
            var YAxis = "R_Stick_V";
            if (controllerName.IndexOf("Generic") != -1)
            {
                XAxis = "R_Stick_H2";
            }
            //回転処理
            if (controllerName.IndexOf("Generic") != -1)
            {
                x += Input.GetAxis(XAxis) * xSpeed * sensix1;//PS4の10倍 0.12
                y += Input.GetAxis(YAxis) * ySpeed * sensiy1;//PS4の10倍 0.08
            }
            else
            {
                x += Input.GetAxis(XAxis) * xSpeed * sensix2;//PS4の10倍 0.02
                y += Input.GetAxis(YAxis) * ySpeed * sensiy2;//PS4の10倍 0,02
            }
            y = ClampAngle(y, yMinLimit, 50);//上下の限度

            var rotation = Quaternion.Euler(y, x, 0);
            var position = rotation * new Vector3(0.0f, -0.4f, -distance) + target.position;

            position.y = 2.6f;

            transform.rotation = rotation;
            transform.position = position;
        }

        //タイトルシーンでないなら処理を行う
        //10秒後にCameraEffectのenebledをtrueに
        if (!Cameraef.enabled && SceneManager.GetActiveScene().name != "TitleScene" &&
            SceneManager.GetActiveScene().name != "TutorialScene") Nowtime += Time.deltaTime;

        //if (Nowtime > Limittime && !Cameraef.enabled)
        //{
        //    Cameraef.enabled = true;
        //}

        //if (Input.GetButton("Fire1"))
        //{
        //    Cameraef.enabled = true;
        //    Cameraef.Closeeye();
        //}

        //if (Input.GetButtonUp("Fire1") && Cameraef.enabled)
        //{
        //    Cameraef.Set_iteration();
        //    Cameraef.enabled = false;
        //    Nowtime = 0;
        //}
    }

    public void SetMoveFlg(bool Flg)
    {
        Moveflg = Flg;
    }

    public bool GetWaitStart()
    {
        return waitstart;
    }

    //カメラの回転を行う
    static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360) { angle += 360; }
        if (angle > 360) { angle -= 360; }
        return Mathf.Clamp(angle, min, max);
    }

    public void GameoverSky()
    {
        Eyeclose = true;
        Closeeye.GetComponent<EyeClose>().CloseAnimation(true);
        Skyboxchange.GetComponent<SkyboxChange>().ChangeMaterial();
    }
}