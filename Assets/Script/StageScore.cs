﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageScore : MonoBehaviour
{
    private int Escore;
    private int Nscore;
    private int Hscore;

    private GameObject[] Gravenum;

    // Start is called before the first frame update
    void Start()
    {
        if(gameObject.name == "EImage")
        {
            if (PlayerPrefs.GetInt("EasyMaxScore") < PlayerPrefs.GetInt("EResult")) 
            {
                Escore = PlayerPrefs.GetInt("EResult");

                PlayerPrefs.SetInt("EasyMaxScore", Escore);
                PlayerPrefs.Save();
            }
            else Escore = PlayerPrefs.GetInt("EasyMaxScore");

            Gravenum = new GameObject[5];
            for(int i = 0; i < 5; i++)
            {
                Gravenum[i] = transform.GetChild(i).gameObject;

                if (Escore < i + 1) Gravenum[i].SetActive(false);
            }
        }
        else if (gameObject.name == "NImage")
        {
            if (PlayerPrefs.GetInt("NormalMaxScore") < PlayerPrefs.GetInt("NResult"))
            { 
                Nscore = PlayerPrefs.GetInt("NResult");

                PlayerPrefs.SetInt("NormalMaxScore", Nscore);
                PlayerPrefs.Save();
            }
            else Escore = PlayerPrefs.GetInt("NormalMaxScore");

            Gravenum = new GameObject[5];
            for (int i = 0; i < 5; i++)
            {
                Gravenum[i] = transform.GetChild(i).gameObject;

                if(Nscore < i + 1) Gravenum[i].SetActive(false);
            }
        }
        else if (gameObject.name == "HImage")
        {
            if(PlayerPrefs.GetInt("HardMaxScore") < PlayerPrefs.GetInt("HResult"))
            {
                Hscore = PlayerPrefs.GetInt("HResult");

                PlayerPrefs.SetInt("HardMaxScore", Hscore);
                PlayerPrefs.Save();
            }
            else Escore = PlayerPrefs.GetInt("HardMaxScore");

            Gravenum = new GameObject[5];
            for (int i = 0; i < 5; i++)
            {
                Gravenum[i] = transform.GetChild(i).gameObject;

                if (Hscore < i + 1) Gravenum[i].SetActive(false);
            }
        }
    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.DeleteAll();
    }
}
