﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Mirror;
using XInputDotNetPure;
public class EnemySpawn : NetworkBehaviour
{
    // プレイヤーの Transformコンポーネントを格納する変数
    public Transform target;
    private bool Onlineflg;
    private bool NPCSpawnflg;
    private float Enemyinterval = 15f;//処理を何秒ごとにするかを決める変数
    private float EtmpTime = 0;//正確な時間を入れる変数
    private GameObject GhostOblect01;
    private GameObject Ghostcln;
    [SerializeField]
    GameObject ChildrenOblect01;
    GameObject Childcln;
    GameObject Childsp;

    Vector3 PlyChildPos;
    Quaternion PlyChildAngle;

    NetworkChildMove Ncm;
    private bool Ghostflg;//ゴーストの生成
    int RandEnemy;//敵の湧きをランダムにする変数

    // 子供を生成する座標と向き
    private readonly int[,] Enemypos = new int[4, 4]
    { //  x,  y,   z, 向き 
        {30,  0,  0,   -90},
        {0,  0,  30,   180},
        {-30,  0,  0,   90},
        {0,  0, -30, 0}
    };
    // Start is called before the first frame update
    void Start()
    {
        Ghostflg = false;
        if (SceneManager.GetActiveScene().name != "OnlineScene") Onlineflg = false;
        else Onlineflg = true;

        NPCSpawnflg = false;

        //if(!isServer)this.enabled = false;

        if(!Onlineflg) target = GameObject.Find("Player").transform;

        // 墓の生成
        if(!Onlineflg) ChildrenOblect01 = (GameObject)Resources.Load("Prefab/Children with Pickaxe");

        GhostOblect01 = (GameObject)Resources.Load("Prefab/Ghost");
        Childsp = GameObject.Find("ChildrenSpawn");

        PlyChildPos = new Vector3(0, 0, 0);
        PlyChildAngle = new Quaternion();


        switch (StageChange.Stagenum)
        {
            case 1:
                Enemyinterval = 20;
                break;
            case 2:
                Enemyinterval = 14;
                break;
            case 3:
                Enemyinterval = 8;
                break;
            default:
                Enemyinterval = 20;
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //バイブ
        if (EtmpTime <= 0.4 && Ghostflg == true)
        {
            XInputDotNetPure.GamePad.SetVibration(0, 5, 5);
        }
        else
        {
            Ghostflg = false;
            XInputDotNetPure.GamePad.SetVibration(0, 0, 0);
        }
        if (Onlineflg && target == null) target = GameObject.Find("Player(Network)(Clone)").transform;

        // 変数 targetPos を作成してターゲットオブジェクトの座標を格納
        Vector3 targetPos = target.position;

        //if(Ncm == null && Onlineflg) Ncm = GameObject.Find("Children(Network)(Clone)").GetComponent<NetworkChildMove>();

        EtmpTime += Time.deltaTime;
        EtmpTime += Time.deltaTime;

        //子供の生成
        if (EtmpTime >= Enemyinterval)
        {
            if(0 <= targetPos.x && 0 <= targetPos.z)
            {
                if (targetPos.x <= targetPos.z)
                {
                    if(Onlineflg) CmdIfOnlineSpawnEnemy(ChildrenOblect01, new Vector3(Enemypos[3, 0], Enemypos[3, 1], Enemypos[3, 2]), 
                        Quaternion.AngleAxis(Enemypos[3, 3], new Vector3(0.0f, 1.0f, 0.0f)));

                    else SpawnEnemy(new Vector3(Enemypos[3, 0], Enemypos[3, 1], Enemypos[3, 2]), 
                        Quaternion.AngleAxis(Enemypos[3, 3], new Vector3(0.0f, 1.0f, 0.0f)));
                }
                else
                {
                    if(Onlineflg) CmdIfOnlineSpawnEnemy(ChildrenOblect01, new Vector3(Enemypos[2, 0], Enemypos[2, 1], Enemypos[2, 2]), 
                        Quaternion.AngleAxis(Enemypos[2, 3], new Vector3(0.0f, 1.0f, 0.0f)));

                    else SpawnEnemy(new Vector3(Enemypos[2, 0], Enemypos[2, 1], Enemypos[2, 2]), 
                        Quaternion.AngleAxis(Enemypos[2, 3], new Vector3(0.0f, 1.0f, 0.0f)));
                }
            }
            else if(0 >= targetPos.x && 0 <= targetPos.z)
            {
                if (targetPos.x <= targetPos.z)
                {
                    if(Onlineflg) CmdIfOnlineSpawnEnemy(ChildrenOblect01, new Vector3(Enemypos[3, 0], Enemypos[3, 1], Enemypos[3, 2]), 
                        Quaternion.AngleAxis(Enemypos[3, 3], new Vector3(0.0f, 1.0f, 0.0f)));

                    else SpawnEnemy(new Vector3(Enemypos[3, 0], Enemypos[3, 1], Enemypos[3, 2]), 
                        Quaternion.AngleAxis(Enemypos[3, 3], new Vector3(0.0f, 1.0f, 0.0f)));
                }
                else
                {
                    if(Onlineflg) CmdIfOnlineSpawnEnemy(ChildrenOblect01, new Vector3(Enemypos[0, 0], Enemypos[0, 1], Enemypos[0, 2]), 
                        Quaternion.AngleAxis(Enemypos[0, 3], new Vector3(0.0f, 1.0f, 0.0f)));

                    else SpawnEnemy(new Vector3(Enemypos[0, 0], Enemypos[0, 1], Enemypos[0, 2]), 
                        Quaternion.AngleAxis(Enemypos[0, 3], new Vector3(0.0f, 1.0f, 0.0f)));
                }
            }
            else if (0 <= targetPos.x && 0 >= targetPos.z)
            {
                targetPos.z = targetPos.z * -1;
                if (targetPos.x <= targetPos.z)
                {
                    if (Onlineflg) CmdIfOnlineSpawnEnemy(ChildrenOblect01, new Vector3(Enemypos[1, 0], Enemypos[1, 1], Enemypos[1, 2]), 
                        Quaternion.AngleAxis(Enemypos[1, 3], new Vector3(0.0f, 1.0f, 0.0f)));

                    else SpawnEnemy(new Vector3(Enemypos[1, 0], Enemypos[1, 1], Enemypos[1, 2]), 
                        Quaternion.AngleAxis(Enemypos[1, 3], new Vector3(0.0f, 1.0f, 0.0f)));
                }
                else
                {
                    if(Onlineflg) CmdIfOnlineSpawnEnemy(ChildrenOblect01, new Vector3(Enemypos[2, 0], Enemypos[2, 1], Enemypos[2, 2]), 
                        Quaternion.AngleAxis(Enemypos[2, 3], new Vector3(0.0f, 1.0f, 0.0f)));

                    else SpawnEnemy(new Vector3(Enemypos[2, 0], Enemypos[2, 1], Enemypos[2, 2]), 
                        Quaternion.AngleAxis(Enemypos[2, 3], new Vector3(0.0f, 1.0f, 0.0f)));
                }
            }
            else if (0 >= targetPos.x && 0 >= targetPos.z)
            {
                targetPos.z = targetPos.z * -1;
                 if (targetPos.x <= targetPos.z)
                 {
                    if(Onlineflg) CmdIfOnlineSpawnEnemy(ChildrenOblect01, new Vector3(Enemypos[1, 0], Enemypos[1, 1], Enemypos[1, 2]), 
                        Quaternion.AngleAxis(Enemypos[1, 3], new Vector3(0.0f, 1.0f, 0.0f)));

                    else SpawnEnemy(new Vector3(Enemypos[1, 0], Enemypos[1, 1], Enemypos[1, 2]), 
                        Quaternion.AngleAxis(Enemypos[1, 3], new Vector3(0.0f, 1.0f, 0.0f)));
                }
                 else
                 {
                    if(Onlineflg) CmdIfOnlineSpawnEnemy(ChildrenOblect01, new Vector3(Enemypos[0, 0], Enemypos[0, 1], Enemypos[0, 2]), 
                        Quaternion.AngleAxis(Enemypos[0, 3], new Vector3(0.0f, 1.0f, 0.0f)));

                    else SpawnEnemy(new Vector3(Enemypos[0, 0], Enemypos[0, 1], Enemypos[0, 2]), 
                        Quaternion.AngleAxis(Enemypos[0, 3], new Vector3(0.0f, 1.0f, 0.0f)));
                }
            }
            //Debug.Log("15秒経過");
            EtmpTime = 0;
        }

            //クライアント側からのスポーン
            //if(NPCSpawnflg) CmdIfOnlineSpawnEnemy(ChildrenOblect01, PlyChildPos, PlyChildAngle);
      }
    
    private void SpawnEnemy(Vector3 Enemypos, Quaternion Angle)
    {
        RandEnemy = Random.Range(0, 2);
        if (RandEnemy == 0)
        {
            Childcln = Instantiate(ChildrenOblect01, Enemypos, Angle);
            Childcln.transform.SetParent(this.gameObject.transform, false);
        }
        else
        {
            Ghostflg = true;
            Ghostcln = Instantiate(GhostOblect01, Enemypos, Angle);
            Ghostcln.transform.SetParent(this.gameObject.transform, false);
        }

    }
    public void SetSpawnPos(Vector3 Pos, Quaternion Angle, bool Flg)
    {
        PlyChildPos = Pos;
        PlyChildAngle = Angle;
        NPCSpawnflg = Flg;
    }

    [ServerCallback]
    private void CmdIfOnlineSpawnEnemy(GameObject ChildObject, Vector3 Childpos, Quaternion Angle)
    {
        Childcln = Instantiate(ChildObject, Childpos, Angle);
        Childcln.GetComponent<ChildrenController>().SetOnlineFlg(Onlineflg);
        Childcln.transform.SetParent(this.gameObject.transform, true);
        NetworkServer.Spawn(Childcln);
    }
    
    public bool GetOnlineFlg()
    {
        return SceneManager.GetActiveScene().name == "OnlineScene";
    }

    public void SetGhostFlg(bool Flg)
    {
        Ghostflg = Flg;
    }
}
